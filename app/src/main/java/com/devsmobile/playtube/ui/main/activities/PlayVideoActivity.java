package com.devsmobile.playtube.ui.main.activities;


import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.devsmobile.playtube.R;
import com.devsmobile.playtube.data.adapter.PlayListRecyclerAdapter;
import com.devsmobile.playtube.data.adapter.VideoSearchRecyclerAdapter;
import com.devsmobile.playtube.data.local.SQLiteDatabaseHandler;
import com.devsmobile.playtube.data.model.Main;
import com.devsmobile.playtube.data.model.Video;
import com.devsmobile.playtube.data.remote.BitmapImage;
import com.devsmobile.playtube.data.remote.VideoRequest;
import com.devsmobile.playtube.data.services.ScreenReceiver;
import com.devsmobile.playtube.inyection.callback.IDataVideo;
import com.devsmobile.playtube.inyection.callback.IGetAds;
import com.devsmobile.playtube.inyection.callback.IGetImage;
import com.devsmobile.playtube.inyection.callback.IOpenDialogChannel;
import com.devsmobile.playtube.inyection.callback.IOpenPlayList;
import com.devsmobile.playtube.inyection.callback.IPlayVideo;
import com.devsmobile.playtube.ui.main.Common;
import com.devsmobile.playtube.ui.main.fragments.PlayListFragment;
import com.devsmobile.playtube.util.Global;
import com.devsmobile.playtube.util.HelperData;
import com.devsmobile.playtube.util.Image;
import com.devsmobile.playtube.util.SnackBar;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.makeramen.roundedimageview.RoundedImageView;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubView;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;

import java.util.ArrayList;
import java.util.List;

import static com.devsmobile.playtube.util.Global.NOTIFICATION_ID;


public class PlayVideoActivity extends YouTubeBaseActivity {

    static NotificationManager notificationManager;
    static NotificationCompat.Builder builder;
    static RemoteViews notificationView;
    static String urlImageNotification;
    static String titleVideoNotification;
    static String channelVideoNotification;
    private String videoId;
    private RecyclerView recyclerView;
    private TextView tvLike;
    private TextView tvDislike;
    private TextView tvView;
    private TextView tvTitle;
    private TextView tvChannelName;
    private TextView tvSubscriber;
    private RoundedImageView imgChannel;
    private RelativeLayout layoutProgress;
    private ProgressBar progressBar, progressBarBottom;
    private YouTubePlayerView youTubePlayerView;

    private List<Object> currentDataVideos;
    private int sizeItemData;

    private boolean isGettingData = false;
    private String currentTitle;
    private String pageToken;
    private NestedScrollView nestedScrollView;
    private int sizeNullValues = 0;
    private VideoSearchRecyclerAdapter adapter;
    private List<Object> detailPlayLists;
    private FirebaseAnalytics firebaseAnalytics;
    private static Bundle currentBundle ;
    private static int currentSecond = 0 ;
    private MoPubView moPubView;

    private static void changeViewNotification(String packageName, boolean isFirstInit, Context context,
                                               boolean changeStatePlayer, boolean clickInNotification) {
        try {
            if (notificationView == null) {
                notificationView = new RemoteViews(packageName, R.layout.notification_expanded);

                Intent intent = new Intent(context, PlayVideoActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra("CURRENT_SECOND", currentSecond);
                intent.putExtras(currentBundle);
                PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                builder = new NotificationCompat.Builder(context, Global.CHANNEL_NOTIFICATION_ID)
                        .setSmallIcon(R.drawable.ic_play_circle_filled_black_24dp) // notification_condensed icon
                        .setCustomContentView(notificationView)
                        .setAutoCancel(false) // clear notification_condensed after click
                        .setContentIntent(pendingIntent);

                Intent switchIntent = new Intent(context, ButtonNotificationListener.class);
                PendingIntent pendingSwitchIntent = PendingIntent.getBroadcast(context, 0,
                        switchIntent, 0);

                notificationView.setOnClickPendingIntent(R.id.btn_play,
                        pendingSwitchIntent);

                notificationView.setOnClickPendingIntent(R.id.btn_pause,
                        pendingSwitchIntent);


            }
            if (Global.STATE_VIDEO.equals(Global.PLAYING)) {

                if (isFirstInit) {
                    notificationView.setViewVisibility(R.id.btn_play, View.GONE);
                    notificationView.setViewVisibility(R.id.btn_pause, View.VISIBLE);
                } else {
                    visibilityIcons(Global.STATE_VIDEO, changeStatePlayer, clickInNotification);
                }

            } else if (Global.STATE_VIDEO.equals(Global.PAUSED)) {
                visibilityIcons(Global.STATE_VIDEO, changeStatePlayer, clickInNotification);
            }
            notificationView.setTextViewText(R.id.tv_channel_video, channelVideoNotification);

            if (isFirstInit) {

                notificationView.setTextViewText(R.id.tv_title_video, titleVideoNotification);
                new BitmapImage().execute(urlImageNotification, new IGetImage() {
                    @Override
                    public void onGetImage(Bitmap bitmap) {
                        notificationView.setImageViewBitmap(R.id.img_notification, bitmap);
                        builder.setCustomContentView(notificationView);
                        assert notificationManager != null;
                        notificationManager.notify(NOTIFICATION_ID, builder.build());
                    }

                    @Override
                    public void onErrorImage(Throwable throwable) {
                        builder.setCustomContentView(notificationView);
                        assert notificationManager != null;
                        notificationManager.notify(NOTIFICATION_ID, builder.build());
                    }
                });
            } else {
                builder.setCustomContentView(notificationView);
                assert notificationManager != null;
                notificationManager.notify(NOTIFICATION_ID, builder.build());
            }


        } catch (Exception e) {
            System.out.println(e);
        }


    }

    private static void visibilityIcons(String state, boolean changeStatePlayer, boolean clickInNotification) {
        int visibilityPlay;
        int visibilityPause;
        if (changeStatePlayer) {
            if (state.equals(Global.PAUSED))
                Global.YOUTUBE_PLAYER.play();
            else
                Global.YOUTUBE_PLAYER.pause();
        }

        if (state.equals(Global.PAUSED)) {
            visibilityPlay = View.VISIBLE;
            visibilityPause = View.GONE;
        } else {
            visibilityPlay = View.GONE;
            visibilityPause = View.VISIBLE;
        }
        if (!clickInNotification) {
            notificationView.setViewVisibility(R.id.btn_play, visibilityPlay);
            notificationView.setViewVisibility(R.id.btn_pause, visibilityPause);
        } else {
            notificationView.setViewVisibility(R.id.btn_play, visibilityPause);
            notificationView.setViewVisibility(R.id.btn_pause, visibilityPlay);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_video);
        if (youTubePlayerView != null) {
            youTubePlayerView.release();
        }
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        this.tvLike = findViewById(R.id.tv_like);
        this.tvDislike = findViewById(R.id.tv_dislike);
        this.tvView = findViewById(R.id.tv_view);
        this.tvTitle = findViewById(R.id.tv_title);
        this.layoutProgress = findViewById(R.id.layout_progress);
        this.progressBar = findViewById(R.id.progress_bar);
        this.progressBarBottom = findViewById(R.id.progress_bar_bottom);
        this.imgChannel = findViewById(R.id.img_channel);
        this.tvChannelName = findViewById(R.id.tv_channel_name);
        this.tvSubscriber = findViewById(R.id.tv_subscriber);
        this.nestedScrollView = findViewById(R.id.container_recycler);
        currentSecond = 0;
        TextView tvRelatedVideo = findViewById(R.id.tv_related_video);



        Bundle bundle = getIntent().getExtras();
        currentBundle = bundle;
        if (bundle != null) {
            this.videoId = bundle.getString("VIDEO_ID", "");
            this.tvLike.setText(new HelperData().getNumberFormatter(bundle.getLong("VIDEO_LIKE", Global.ZERO_VALUE)));
            this.tvDislike.setText(new HelperData().getNumberFormatter(bundle.getLong("VIDEO_DISLIKE", Global.ZERO_VALUE)));
            this.tvView.setText(new HelperData().getNumberFormatter(bundle.getLong("VIDEO_VIEW", Global.ZERO_VALUE)));
            this.tvTitle.setText(bundle.getString("VIDEO_TITLE", ""));
            titleVideoNotification = tvTitle.getText().toString();
            urlImageNotification = bundle.getString("VIDEO_URL_THUMBNAIL", "");

            Video video = new Video();
            video.setIdVideoYouTube(videoId);
            video.setTitleVideo(tvTitle.getText().toString());
            video.setUrlThumbnail(urlImageNotification);
            video.setIdChannel(bundle.getString("CHANNEL_ID", Global.EMPTY_STRING));
            this.getInfoChannel(bundle.getString("CHANNEL_ID", Global.EMPTY_STRING), video);
            if (bundle.getBoolean("GET_DATA_VIDEO", false)) {
                getInfoVideo(this.videoId);
            }
            this.currentTitle = bundle.getString("VIDEO_TITLE", Global.EMPTY_STRING);


        }
        this.youTubePlayerView = findViewById(R.id.youtube_player_view);

        this.initializeYoutubePlayer(videoId);

        this.recyclerView = findViewById(R.id.recycler_search);
        this.recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        this.recyclerView.setLayoutManager(linearLayoutManager);

        // iLoading.onInitLoading();

        if (bundle != null) {

            if (!bundle.getString("ID_PLAY_LIST", Global.EMPTY_STRING).equals(Global.EMPTY_STRING)) {
                SQLiteDatabaseHandler databaseHandler = new SQLiteDatabaseHandler(this);
                detailPlayLists = new ArrayList<>(databaseHandler.getDetailPlayList(
                        Integer.parseInt(bundle.getString("ID_PLAY_LIST", "0"))));

                tvRelatedVideo.setText(bundle.getString("TITLE_PLAY_LIST", Global.EMPTY_STRING));
                //  populateRecyclerViewPlayList(addNullValues( new ArrayList<>(detailPlayLists)));
                populateRecyclerViewPlayList(new ArrayList<>(detailPlayLists));

            } else {
                this.getQueryData(true);

                nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
                    @Override
                    public void onScrollChanged() {
                        View view = nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                        int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
                                .getScrollY()));

                        if (diff == 0) {
                            if (!isGettingData) {
                                isGettingData = true;
                                if (sizeItemData < 7) {

                                    Bundle params = new Bundle();
                                    params.putString("get_more_data_playing", videoId == null ? "" : videoId);
                                    firebaseAnalytics.logEvent("get_more_data_playing", params);

                                    progressBarBottom.setVisibility(View.VISIBLE);
                                    getQueryData(false);
                                }
                            }
                        }
                    }
                });


            }
        }

        if (!Global.PUBLISH_TO_PLAY_STORE) {
            IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
            filter.addAction(Intent.ACTION_SCREEN_OFF);
            BroadcastReceiver mReceiver = new ScreenReceiver();
            this.registerReceiver(mReceiver, filter);
        }


        Bundle params = new Bundle();
        params.putString("idVideo", videoId == null ? "" : videoId);
        firebaseAnalytics.logEvent("play_video", params);

        moPubView = (MoPubView) findViewById(R.id.mopub_view);
        moPubView.setAdUnitId(Global.APP_ID_MOPUB);
        moPubView.loadAd();
        moPubView.setBannerAdListener(new MoPubView.BannerAdListener() {
            @Override
            public void onBannerLoaded(@NonNull MoPubView banner) {

            }

            @Override
            public void onBannerFailed(MoPubView banner, MoPubErrorCode errorCode) {
                System.out.println("Faild");
            }

            @Override
            public void onBannerClicked(MoPubView banner) {
                System.out.println("CLicked");
            }

            @Override
            public void onBannerExpanded(MoPubView banner) {
                System.out.println("Expended");
            }

            @Override
            public void onBannerCollapsed(MoPubView banner) {
                System.out.println("Collapsed");
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (notificationManager != null) {
            notificationManager.cancel(NOTIFICATION_ID);
            notificationManager = null;
        }
    }


    private void getQueryData(boolean showProgress) {
        if (showProgress)
            this.showProgress();

        VideoRequest request = new VideoRequest(new IDataVideo() {
            @Override
            public void onGetDataVideoSuccess(Main main) {
                pageToken = main.getNextPageToken();
                if (currentDataVideos == null) {
                    currentDataVideos = new ArrayList<>(main.getItems());
                    sizeItemData = 0;
                    populateRecyclerView(currentDataVideos, recyclerView);
                } else {
                    currentDataVideos.addAll(main.getItems());
                    adapter.updateRecycler(new ArrayList<>(currentDataVideos));

                }
                sizeItemData += main.getItems().size();
                sizeItemData += sizeNullValues;
                hideProgress();
                isGettingData = false;
            }

            @Override
            public void onGetDataVideoFailure(Throwable t) {

                isGettingData = false;
                SnackBar.errorData(PlayVideoActivity.this);
                hideProgress();

            }
        });
        request.getQueryVideoData(currentTitle, videoId, pageToken, 0);

    }

    private void getInfoChannel(String idVideo, Video video) {
        VideoRequest videoRequest = new VideoRequest(new IDataVideo() {
            @Override
            public void onGetDataVideoSuccess(Main main) {
                if (main.getItems().size() > 0) {
                    channelVideoNotification = main.getItems().get(0).getSnippet().getTitle();
                    tvChannelName.setText(channelVideoNotification);
                    video.setChannelName(channelVideoNotification);
                    video.setType(Global.TYPE_VIDEO_HISTORY);
                    addVideo(video);
                    Image.loadImage(imgChannel, main.getItems().get(0).getSnippet().getThumbnails().getMedium().getUrl());

                    long subscribers = Long.parseLong(main.getItems().get(0).getStatistics().getSubscriberCount());
                    tvSubscriber.setText(new HelperData().getNumberFormatter(subscribers) + " " + getResources().getString(R.string.subscribers));
                }
                if (!Global.PUBLISH_TO_PLAY_STORE)
                    changeViewNotification(getPackageName(), false, getApplicationContext(), false, false);
            }

            @Override
            public void onGetDataVideoFailure(Throwable t) {
                SnackBar.errorData(PlayVideoActivity.this);
            }
        });
        videoRequest.getChannelData(idVideo);
    }

    private void getInfoVideo(String idVideo) {
        VideoRequest videoRequest = new VideoRequest(new IDataVideo() {
            @Override
            public void onGetDataVideoSuccess(Main main) {
                tvDislike.setText(new HelperData().getNumberFormatter(Long.parseLong(main.getItems().get(0).getStatistics().getDislikeCount())));
                tvLike.setText(new HelperData().getNumberFormatter(Long.parseLong(main.getItems().get(0).getStatistics().getLikeCount())));
            }

            @Override
            public void onGetDataVideoFailure(Throwable t) {
                SnackBar.errorData(PlayVideoActivity.this);
            }
        });

        videoRequest.getVideoData(idVideo);
        this.progressBarBottom.setVisibility(View.GONE);
    }

    private void populateRecyclerView(List<Object> arrayListItem, RecyclerView recyclerView) {
        adapter = new VideoSearchRecyclerAdapter(getApplicationContext(), arrayListItem, new IPlayVideo() {
            @Override
            public void onPlayVideo(Bundle bundle) {
                Bundle params = new Bundle();
                params.putString("play_video", "");
                firebaseAnalytics.logEvent("play_video_from_playing", params);


                handleChangeVideo(bundle, true);
            }
        }, new IOpenDialogChannel() {
            @Override
            public void onOpenDialogChannel(Bundle bundle) {

            }
        }, new IOpenPlayList() {
            @Override
            public void onShowPlayList(Bundle bundle) {

                PlayListFragment dialog = new PlayListFragment();
                dialog.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                // fragmentTransaction.setCustomAnimations( R.anim.enter_from_right, R.anim.exit_to_right);
                fragmentTransaction.addToBackStack(null);
                //  fragmentTransaction.add(android.R.id.content, dialog);
                fragmentTransaction.commit();


            }
        });
        recyclerView.setAdapter(adapter);
        //handleAdsAdapter(arrayListItem,adapter);
    }

    private void populateRecyclerViewPlayList(List<Object> itemList) {
        nestedScrollView.scrollTo(0, 0);
        PlayListRecyclerAdapter adapter = new PlayListRecyclerAdapter(this, itemList
                , new IPlayVideo() {
            @Override
            public void onPlayVideo(Bundle bundle) {
                getInfoVideo(bundle.getString("VIDEO_ID", Global.EMPTY_STRING));
                handleChangeVideo(bundle, false);

            }
        }, null);

        recyclerView.setAdapter(adapter);
        //handleAdsAdapter(itemList, adapter);

    }

    private void handleChangeVideo(Bundle bundle, boolean isGetRelatedVideo) {
        if (bundle != null) {
            videoId = bundle.getString("VIDEO_ID", null);
            tvLike.setText(new HelperData().getNumberFormatter(bundle.getLong("VIDEO_LIKE", 0)));
            tvDislike.setText(new HelperData().getNumberFormatter(bundle.getLong("VIDEO_DISLIKE", 0)));
            tvView.setText(new HelperData().getNumberFormatter(bundle.getLong("VIDEO_VIEW", 0)));
            tvTitle.setText(bundle.getString("VIDEO_TITLE", null));

            //Cuando se selecciona un video relacionado al  que se reproduce actualmente
            //se empieza a reproducir el nuevo video seleccionado
            if (Global.YOUTUBE_PLAYER != null) {
                Global.YOUTUBE_PLAYER.loadVideo(videoId, 0);
                urlImageNotification = bundle.getString("VIDEO_URL_THUMBNAIL", "");
                nestedScrollView.scrollTo(0, 0);

                Video video = new Video();
                video.setIdVideoYouTube(videoId);
                video.setTitleVideo(tvTitle.getText().toString());
                video.setUrlThumbnail(urlImageNotification);
                video.setIdChannel(bundle.getString("CHANNEL_ID", ""));
                getInfoChannel(bundle.getString("CHANNEL_ID", ""), video);
                titleVideoNotification = tvTitle.getText().toString();
                currentTitle = bundle.getString("VIDEO_TITLE", "");
                currentDataVideos = null;
                sizeItemData = 0;
                getInfoVideo(this.videoId);
                if (!Global.PUBLISH_TO_PLAY_STORE) {
                    showNotification();
                }


            }
            //Se vuelven a obtener los videos relacionados
            if (isGetRelatedVideo)
                getQueryData(true);
        }

    }

    private void initializeYoutubePlayer(String videoID) {


        youTubePlayerView.setEnableAutomaticInitialization(false);
        if (!Global.PUBLISH_TO_PLAY_STORE)
            youTubePlayerView.enableBackgroundPlayback(true);
        youTubePlayerView.initialize(new YouTubePlayerListener() {
            @Override
            public void onReady(@NonNull YouTubePlayer youTubePlayer) {

                youTubePlayer.loadVideo(videoID, currentSecond);
                Global.YOUTUBE_PLAYER = youTubePlayer;
                Global.STATE_VIDEO = Global.PLAYING;
                showNotification();
            }

            @Override
            public void onStateChange(@NonNull YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlayerState playerState) {
                if (playerState.equals(PlayerConstants.PlayerState.PAUSED) || playerState.equals(PlayerConstants.PlayerState.ENDED)) {
                    Global.STATE_VIDEO = Global.PAUSED;
                    if (!Global.PUBLISH_TO_PLAY_STORE)
                        changeViewNotification(getPackageName(), false, getApplicationContext(), false, false);

                } else if (playerState.equals(PlayerConstants.PlayerState.PLAYING)) {
                    Global.STATE_VIDEO = Global.PLAYING;
                    if (!Global.PUBLISH_TO_PLAY_STORE)
                        changeViewNotification(getPackageName(), false, getApplicationContext(), false, false);
                }

            }

            @Override
            public void onPlaybackQualityChange(@NonNull YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlaybackQuality playbackQuality) {

            }

            @Override
            public void onPlaybackRateChange(@NonNull YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlaybackRate playbackRate) {

            }

            @Override
            public void onError(@NonNull YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlayerError playerError) {
                SnackBar.error(PlayVideoActivity.this, getString(R.string.error_play_video));
                hideProgress();
            }

            @Override
            public void onCurrentSecond(@NonNull YouTubePlayer youTubePlayer, float v) {
                System.out.println("CURRENT SECOND : "+ v);
                currentSecond = Math.round(v);
            }

            @Override
            public void onVideoDuration(@NonNull YouTubePlayer youTubePlayer, float v) {

                System.out.println("VIDEO DURATION : " +  v);
            }

            @Override
            public void onVideoLoadedFraction(@NonNull YouTubePlayer youTubePlayer, float v) {

            }

            @Override
            public void onVideoId(@NonNull YouTubePlayer youTubePlayer, String s) {

            }

            @Override
            public void onApiChange(@NonNull YouTubePlayer youTubePlayer) {

            }
        }, true);

    }

    private void showNotification() {

        notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(Global.CHANNEL_NOTIFICATION_ID,
                    Global.CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(Global.CHANNEL_DESCRIPTION);
            assert notificationManager != null;
            notificationManager.createNotificationChannel(channel);


        }
        if (!Global.PUBLISH_TO_PLAY_STORE)
            changeViewNotification(getPackageName(), true, this, true, true);


    }

    @Override
    protected void onPause() {

        Global.PLAY_VIDEO_ACTIVITY_VISIBLE = false;
        if (ScreenReceiver.screenOff) {
            System.out.println("SCREEN TURNED OFF");
        }
        super.onPause();
    }


    @Override
    protected void onResume() {

        super.onResume();
    }

    private void handleAdsAdapter(List<Object> itemList, RecyclerView.Adapter adapter) {
        new Common(getApplicationContext()).getNativeAds(this, new IGetAds() {
            @Override
            public void onGetSuccessAds(List<UnifiedNativeAd> nativeAdList) {
                List<Object> objectList = new ArrayList<>();
                int positionAd = 0;
                for (Object item : itemList) {
                    if (item == null) {
                        objectList.add(nativeAdList.get(positionAd));
                        positionAd++;
                    } else {
                        objectList.add(item);
                    }
                }
                if (adapter instanceof VideoSearchRecyclerAdapter) {
                    currentDataVideos = objectList;
                    VideoSearchRecyclerAdapter recyclerAdapter = (VideoSearchRecyclerAdapter) adapter;
                    recyclerAdapter.updateRecycler(objectList);
                } else {
                    detailPlayLists = objectList;
                    PlayListRecyclerAdapter recyclerAdapter = (PlayListRecyclerAdapter) adapter;
                    recyclerAdapter.updateRecycler(objectList);
                }

            }

            @Override
            public void onError() {
                SnackBar.errorData(getApplicationContext());

            }
        }, sizeNullValues);
    }

    //Agrega valores NULL al array unicamente para guardar la posicion de los anuncios
    private List<Object> addNullValues(List<Object> objectList) {
        int counter = 0;
        sizeNullValues = 0;
        List<Object> newObjectList = new ArrayList<>();
        for (Object item : objectList) {
            if (sizeNullValues < 5) { // Se valida que sea menor que sinco porque solo se van a obtener como maximo 5 nuncios
                if (counter == 0 || counter == 2) {
                    newObjectList.add(null); // Al inicio se agregan como NULL para despues reemplazarlos por anuncios
                    sizeNullValues++;
                    if (counter == 2)
                        counter = 0;
                }
                counter++;
            }
            newObjectList.add(item);
        }

        return newObjectList;
    }


    private void showProgress() {
        layoutProgress.setVisibility(View.VISIBLE);
        progressBar.setVisibility(View.VISIBLE);

    }

    private void hideProgress() {
        layoutProgress.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
        progressBarBottom.setVisibility(View.GONE);

    }

    @Override
    protected void onStop() {

        super.onStop();
    }

    private long addVideo(Video item) {

        SQLiteDatabaseHandler databaseHandler = new SQLiteDatabaseHandler(getApplicationContext());
        return databaseHandler.insertVideo(item);
    }

    public static class ButtonNotificationListener extends BroadcastReceiver {
        private FirebaseAnalytics mFirebaseAnalytics;

        @Override
        public void onReceive(Context context, Intent intent) {
            mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
            Bundle params = new Bundle();
            params.putString("notification_click", "");
            mFirebaseAnalytics.logEvent("notification_click", params);
            if (!Global.PUBLISH_TO_PLAY_STORE)
                changeViewNotification(null, false, context, true, true);

        }
    }
}
