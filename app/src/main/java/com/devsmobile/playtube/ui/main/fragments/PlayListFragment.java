package com.devsmobile.playtube.ui.main.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Objects;

import com.devsmobile.playtube.data.adapter.PlayListRecyclerAdapter;
import com.devsmobile.playtube.data.model.Main;
import com.devsmobile.playtube.data.remote.VideoRequest;
import com.devsmobile.playtube.inyection.callback.IDataVideo;
import com.devsmobile.playtube.inyection.callback.IPlayVideo;
import com.devsmobile.playtube.R;
import com.devsmobile.playtube.ui.main.activities.PlayVideoActivity;
import com.devsmobile.playtube.util.Global;
import com.devsmobile.playtube.util.SnackBar;
import com.google.firebase.analytics.FirebaseAnalytics;

public class PlayListFragment extends Fragment {


    private RecyclerView recyclerView;
    private FirebaseAnalytics firebaseAnalytics;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        super.onCreateView(inflater, container, savedInstanceState);

        View view = inflater.inflate(
                R.layout.fragment_play_list, container, false);


        Bundle bundle = getArguments();
        if (bundle != null) {
            firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
            Bundle params = new Bundle();
            params.putString("play_list_id", bundle.getString("PLAYLIST_ID", Global.EMPTY_STRING) );
            firebaseAnalytics.logEvent("view_play_list", params);

            recyclerView = view.findViewById(R.id.rv_play_list);
            recyclerView.setHasFixedSize(true);
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(linearLayoutManager);
            popularRecyclerView(bundle.getString("PLAYLIST_ID"));

        }

        ImageButton btnBack = view.findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right)
                        .remove(PlayListFragment.this).commit();

                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            }
        });

        return view;
    }

    private void popularRecyclerView(String idPlayList) {


        VideoRequest videoRequest = new VideoRequest(new IDataVideo() {
            @Override
            public void onGetDataVideoSuccess(Main main) {
                PlayListRecyclerAdapter playListRecyclerAdapter = new PlayListRecyclerAdapter(getContext(), new ArrayList<>(main.getItems())
                        , new IPlayVideo() {
                    @Override
                    public void onPlayVideo(Bundle bundle) {

                        Bundle params = new Bundle();
                        params.putString("play_video", "" );
                        firebaseAnalytics.logEvent("play_video_from_playlist", params);

                        Intent intent = new Intent(getActivity(), PlayVideoActivity.class);
                        intent.putExtras(bundle);
                        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        startActivity(intent);
                    }

                }, null);

                recyclerView.setAdapter(playListRecyclerAdapter);
            }

            @Override
            public void onGetDataVideoFailure(Throwable t) {

                SnackBar.errorData(getContext());
            }
        });

        VideoRequest request = new VideoRequest(new IDataVideo() {
            @Override
            public void onGetDataVideoSuccess(Main main) {
                videoRequest.getDataVideoInfo(main, true);

            }

            @Override
            public void onGetDataVideoFailure(Throwable t) {
               SnackBar.errorData(getContext());
            }
        });


        request.getPlayListItemData(idPlayList, Global.MAX_RESULTS_PLAY_LIST);


    }
}


