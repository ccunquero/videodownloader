package com.devsmobile.playtube.ui.main.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.util.ArrayList;
import java.util.List;

import com.devsmobile.playtube.R;
import com.devsmobile.playtube.data.adapter.PlayListRecyclerAdapter;
import com.devsmobile.playtube.data.local.SQLiteDatabaseHandler;
import com.devsmobile.playtube.inyection.callback.IGetAds;
import com.devsmobile.playtube.inyection.callback.IPlayVideo;
import com.devsmobile.playtube.ui.main.Common;
import com.devsmobile.playtube.ui.main.activities.PlayVideoActivity;
import com.devsmobile.playtube.util.Global;
import com.devsmobile.playtube.util.SnackBar;
import com.google.firebase.analytics.FirebaseAnalytics;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;


public class HistoryFragment extends Fragment {

   private int sizeNullValues = 0;
    private AdLoader adLoader;
    private List<UnifiedNativeAd> nativeAdList = new ArrayList<>();
    private LinearLayout containerAnimation ;
    private LottieAnimationView lottieAnimationView;
    private TextView tvDescription ;
    private FirebaseAnalytics firebaseAnalytics;

    public HistoryFragment() {

    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(
                R.layout.fragment_history, container, false);


        containerAnimation = view.findViewById(R.id.container_lottie);
        lottieAnimationView = view.findViewById(R.id.animation_view);
        tvDescription   = view.findViewById(R.id.tv_description_animation);

        RecyclerView recyclerView = view.findViewById(R.id.rv_history);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        populateRecyclerView(recyclerView);

        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        Bundle params = new Bundle();
        params.putString("view_history", "" );
        firebaseAnalytics.logEvent("view_history", params);

        return view;
    }

    private void populateRecyclerView(RecyclerView recyclerView) {
        SQLiteDatabaseHandler databaseHandler = new SQLiteDatabaseHandler(getContext());
        //List<Object> videoList = addNullValues( new ArrayList<>(databaseHandler.getVideos(Global.TYPE_VIDEO_HISTORY)) );
        List<Object> videoList = new ArrayList<>(databaseHandler.getVideos(Global.TYPE_VIDEO_HISTORY)) ;

        if(videoList.size() == 0){

            tvDescription.setText(getActivity().getResources().getString(R.string.empty_history));
            containerAnimation.setVisibility(VISIBLE);
            lottieAnimationView.setAnimation(getResources().getIdentifier("history",
                    "raw", getActivity().getPackageName()));

            lottieAnimationView.setRepeatCount(2);
            lottieAnimationView.playAnimation();
        }else{

            containerAnimation.setVisibility(GONE);
        }

        PlayListRecyclerAdapter playListRecyclerAdapter = new PlayListRecyclerAdapter(getContext(), videoList, new IPlayVideo() {
            @Override
            public void onPlayVideo(Bundle bundle) {

                Bundle params = new Bundle();
                params.putString("play_video", "" );
                firebaseAnalytics.logEvent("play_video_from_history", params);

                Intent intent = new Intent(getActivity(), PlayVideoActivity.class);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);

            }
        },  null);

        recyclerView.setAdapter(playListRecyclerAdapter);
    //  handleAdsAdapter(videoList, playListRecyclerAdapter);

    }

    //Agrega valores NULL al array unicamente para guardar la posicion de los anuncios
    private List<Object> addNullValues(List<Object> objectList){
        int counter = 0;
        sizeNullValues = 0;
        List<Object> newObjectList = new ArrayList<>();
        for (Object item : objectList) {
            newObjectList.add(item);
            if(sizeNullValues < 5){ // Se valida que sea menor que sinco porque solo se van a obtener como maximo 5 nuncios
                if(counter == 0 || counter == 4){
                    newObjectList.add(null); // Al inicio se agregan como NULL para despues reemplazarlos por anuncios
                    sizeNullValues++;
                    if(counter == 4)
                        counter = 0;
                }
                counter++;
            }
        }

        return newObjectList;
    }

    private void handleAdsAdapter(List<Object> itemList, PlayListRecyclerAdapter adapter) {
        int a = 0;



        Common common = new Common(getActivity(), new IGetAds() {
            @Override
            public void onGetSuccessAds(List<UnifiedNativeAd> nativeAdList) {
                List<Object> objectList = new ArrayList<>();
                int positionAd = 0;
                for (Object item : itemList) {
                    if (item == null) {
                        objectList.add(nativeAdList.get(positionAd));
                        positionAd++;
                    } else {
                        objectList.add(item);
                    }
                }
                adapter.updateRecycler(objectList);

            }

            @Override
            public void onError() {
                SnackBar.errorData(getActivity());

            }
        }, sizeNullValues, getContext());

        common.start();


    }
}
