package com.devsmobile.playtube.ui.main.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

import com.devsmobile.playtube.R;
import com.devsmobile.playtube.data.adapter.PlayListRecyclerAdapter;
import com.devsmobile.playtube.data.local.SQLiteDatabaseHandler;
import com.devsmobile.playtube.inyection.callback.IClick;
import com.devsmobile.playtube.inyection.callback.IGetAds;
import com.devsmobile.playtube.inyection.callback.IOpenPlayList;
import com.devsmobile.playtube.inyection.callback.IPlayVideo;
import com.devsmobile.playtube.ui.main.Common;
import com.devsmobile.playtube.ui.main.activities.PlayVideoActivity;
import com.devsmobile.playtube.util.Global;
import com.devsmobile.playtube.util.SnackBar;
import com.google.firebase.analytics.FirebaseAnalytics;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class CollectionListFragment extends Fragment {

    private FloatingActionButton fab;
    private RecyclerView rvPlayList;
    private RecyclerView rvFavorites;
    private RecyclerView rvSeeLater;
    private int sizeNullValues =0;
    private LinearLayout  containerAnimation ;
    private LottieAnimationView lottieAnimationView;
    private TextView tvDescription ;
    private FirebaseAnalytics firebaseAnalytics;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_collection_list, container, false);

        fab = view.findViewById(R.id.fab);
        rvPlayList = view.findViewById(R.id.rv_play_list);
        rvFavorites = view.findViewById(R.id.rv_favorites);
        rvSeeLater = view.findViewById(R.id.rv_see_later);
        containerAnimation = view.findViewById(R.id.container_lottie);
        lottieAnimationView = view.findViewById(R.id.animation_view);
        tvDescription   = view.findViewById(R.id.tv_description_animation);

        rvPlayList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvFavorites.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvSeeLater.setLayoutManager(new LinearLayoutManager(getActivity()));

        rvPlayList.setHasFixedSize(true);
        NavigationTabStrip tabStrip = view.findViewById(R.id.tabs_videos);

        initRecyclerByTypeVideo(Global.TYPE_VIDEO_FAVORITE, rvFavorites);
        initRecyclerByTypeVideo(Global.TYPE_VIDEO_SEE_LATER, rvSeeLater);
        populateRecyclerPlayList();
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());

        fab.setOnClickListener(v -> {
            new Common(getContext()).showDialogCreatePlayList(getActivity(), new IClick() {
                @Override
                public void onClickListener(Object object) {
                    long responseAddPlayList = (long) object;
                    if (responseAddPlayList > 0) {

                        SnackBar.success(getActivity(), getResources().getString(R.string.play_list_created_success));
                        populateRecyclerPlayList();
                    } else {
                        SnackBar.error(getActivity(), getResources().getString(R.string.play_list_created_error));
                    }
                }
            });


        });

        tabStrip.setTabIndex(0, true);

        if (Global.CURRENT_TAB_INDEX_SELECTED == 0)
            visibleRecycler(rvPlayList);
        else if (Global.CURRENT_TAB_INDEX_SELECTED == 1)
            visibleRecycler(rvFavorites);
        else if (Global.CURRENT_TAB_INDEX_SELECTED == 2)
            visibleRecycler(rvSeeLater);


        tabStrip.setOnTabStripSelectedIndexListener(new NavigationTabStrip.OnTabStripSelectedIndexListener() {
            @Override
            public void onStartTabSelected(String title, int index) {

                Global.CURRENT_TAB_INDEX_SELECTED = index;
                if (index == 0) { // Playlist
                    visibleRecycler(rvPlayList);
                    populateRecyclerPlayList();
                    Bundle params = new Bundle();
                    params.putString("view_collection_playlist", "" );
                    firebaseAnalytics.logEvent("view_collection_playlist", params);
                } else if (index == 1) { // Favorites
                    visibleRecycler(rvFavorites);
                    initRecyclerByTypeVideo(Global.TYPE_VIDEO_FAVORITE, rvFavorites);
                    Bundle params = new Bundle();
                    params.putString("view_collection_favorites", "" );
                    firebaseAnalytics.logEvent("view_collection_favorites", params);
                } else if (index == 2) { // See later
                    visibleRecycler(rvSeeLater);
                    initRecyclerByTypeVideo(Global.TYPE_VIDEO_SEE_LATER, rvSeeLater);
                    Bundle params = new Bundle();
                    params.putString("view_collection_see_later", "" );
                    firebaseAnalytics.logEvent("view_collection_see_later", params);
                }
            }

            @Override
            public void onEndTabSelected(String title, int index) {

            }
        });

        return view;
    }

    //Obtiene los PlayList generados por el usuario
    private void populateRecyclerPlayList() {
        SQLiteDatabaseHandler sqLiteDatabaseHandler = new SQLiteDatabaseHandler(getContext());
      //  List<Object> playLists = addNullValues( new ArrayList<>( sqLiteDatabaseHandler.getPlayLists()));
        List<Object> playLists = new ArrayList<>( sqLiteDatabaseHandler.getPlayLists());

        if(playLists.size() == 0){

            tvDescription.setText(getActivity().getResources().getString(R.string.empty_playlist));
            containerAnimation.setVisibility(VISIBLE);
            lottieAnimationView.setAnimation(getResources().getIdentifier("playlist",
                    "raw", getActivity().getPackageName()));

            lottieAnimationView.playAnimation();
        }else{

            containerAnimation.setVisibility(GONE);
        }

        PlayListRecyclerAdapter playListRecyclerAdapter = new PlayListRecyclerAdapter(getContext(), playLists, null, new IOpenPlayList() {
            @Override
            public void onShowPlayList(Bundle bundle) {

                DetailPlayListFragment dialog = new DetailPlayListFragment();
                dialog.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.add(android.R.id.content, dialog);
                fragmentTransaction.commit();

            }
        });

        rvPlayList.setAdapter(playListRecyclerAdapter);
     //   handleAdsAdapter(playLists, playListRecyclerAdapter);
    }

    // Los tipos de videos que podra obtener son de Favoritos y ver mas tarde.
    private void initRecyclerByTypeVideo(String type, RecyclerView recyclerView) {



        SQLiteDatabaseHandler databaseHandler = new SQLiteDatabaseHandler(getActivity());
        List<Object> videoList =  new ArrayList<>(databaseHandler.getVideos(type))  ;

        if(type.equals(Global.TYPE_VIDEO_FAVORITE) && videoList.size() == 0){
            tvDescription.setText(getActivity().getResources().getString(R.string.empty_favorite));
            containerAnimation.setVisibility(VISIBLE);
            lottieAnimationView.setAnimation(getResources().getIdentifier("favorite",
                    "raw", getActivity().getPackageName()));
            lottieAnimationView.playAnimation();
        }else if (type.equals(Global.TYPE_VIDEO_SEE_LATER) && videoList.size() == 0){
            tvDescription.setText(getActivity().getResources().getString(R.string.empty_see_later));
            containerAnimation.setVisibility(VISIBLE);
            lottieAnimationView.setAnimation(getResources().getIdentifier("see_later",
                    "raw", getActivity().getPackageName()));
            lottieAnimationView.playAnimation();
        }else{
            containerAnimation.setVisibility(GONE);
        }


        PlayListRecyclerAdapter recyclerAdapter = new PlayListRecyclerAdapter(getContext(), videoList, new IPlayVideo() {
            @Override
            public void onPlayVideo(Bundle bundle) {
                Intent intent = new Intent(getActivity(), PlayVideoActivity.class);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);

            }
        }, null);

        recyclerView.setAdapter(recyclerAdapter);
     //   handleAdsAdapter(videoList, recyclerAdapter);

    }

    private void handleAdsAdapter(List<Object> itemList, PlayListRecyclerAdapter adapter){
        new Common(getContext()).getNativeAds(getActivity(), new IGetAds() {
            @Override
            public void onGetSuccessAds(List<UnifiedNativeAd> nativeAdList) {
                List<Object> objectList = new ArrayList<>();
                int positionAd = 0;
                for (Object item:itemList) {
                    if(item == null){
                        objectList.add(nativeAdList.get(positionAd));
                        positionAd++;
                    }else{
                        objectList.add(item);
                    }
                }
                adapter.updateRecycler(objectList);
            }

            @Override
            public void onError() {
                SnackBar.errorData(getActivity());

            }
        },sizeNullValues);
    }

    private void visibleRecycler(RecyclerView recyclerView) {
        Animation fadeEnter = AnimationUtils.loadAnimation(getContext(), R.anim.fade_enter);
        rvPlayList.setVisibility(GONE);
        rvFavorites.setVisibility(GONE);
        rvSeeLater.setVisibility(GONE);
        recyclerView.setVisibility(VISIBLE);
        recyclerView.startAnimation(fadeEnter);

        if (recyclerView != rvPlayList)
            fab.setVisibility(GONE);
        else
            fab.setVisibility(VISIBLE);

    }

    //Agrega valores NULL al array unicamente para guardar la posicion de los anuncios
    private List<Object> addNullValues(List<Object> objectList){
        int counter = 0;
        sizeNullValues = 0;
        List<Object> newObjectList = new ArrayList<>();
        for (Object item : objectList) {
            newObjectList.add(item);
            if(sizeNullValues < 5){ // Se valida que sea menor que sinco porque solo se van a obtener como maximo 5 nuncios
                if(counter == 0 || counter == 3){
                    newObjectList.add(null); // Al inicio se agregan como NULL para despues reemplazarlos por anuncios
                    sizeNullValues++;
                    if(counter == 3)
                        counter = 0;
                }
                counter++;
            }
        }

        return newObjectList;
    }



}
