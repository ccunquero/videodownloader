package com.devsmobile.playtube.ui.main;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.formats.NativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.android.gms.ads.formats.UnifiedNativeAdView;
import com.rengwuxian.materialedittext.MaterialEditText;



import java.util.ArrayList;
import java.util.List;

import com.devsmobile.playtube.data.local.SQLiteDatabaseHandler;
import com.devsmobile.playtube.data.model.PlayList;
import com.devsmobile.playtube.inyection.callback.IClick;
import com.devsmobile.playtube.R;
import com.devsmobile.playtube.inyection.callback.IGetAds;

public class Common  extends  Thread{

    private List<UnifiedNativeAd> nativeAdList = new ArrayList<>();


    private Activity activity;
    private IGetAds iGetAds;
    private int sizeAds;
    private int counter  = 0;
    private Context context;
    public Common(Context context){
        this.context = context;
    }


    public Common(Activity activity, IGetAds iGetAds, int sizeAds, Context context){
        this.activity = activity;
        this.iGetAds = iGetAds;
        this.sizeAds = sizeAds;
        this.context  = context;

    }

    public void showDialogCreatePlayList(Activity activity, IClick iClick) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity, android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        LayoutInflater inflater1 = activity.getLayoutInflater();
        View dialogView = inflater1.inflate(R.layout.dialog_add_play_list, null);
        dialogBuilder.setView(dialogView);

        MaterialEditText namePlayList = dialogView.findViewById(R.id.tv_title_play_list);
        Button btnAdd = dialogView.findViewById(R.id.btn_add_playlist);
        Button btnCancel = dialogView.findViewById(R.id.btn_cancel);
        namePlayList.requestFocus();


        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
        btnCancel.setOnClickListener(v12 -> alertDialog.hide());
        btnAdd.setOnClickListener(v1 -> {
            long response = addPlayList(namePlayList.getText().toString(), activity);
            alertDialog.hide();
            iClick.onClickListener(response);

        });


    }

    private long addPlayList(String titlePlayList, Activity activity) {

        SQLiteDatabaseHandler sql = new SQLiteDatabaseHandler(activity);
        PlayList playList = new PlayList();
        playList.setUrlThumbnail(null);
        playList.setTitle(titlePlayList);
        return sql.insertPlayList(playList);


    }
    @Override
    public void run() {
        super.run();


    }

    public void getNativeAds(Activity activity, IGetAds iGetAds, int sizeAds) {
        MobileAds.initialize(context);
        counter = 0;
        if(sizeAds == 0) return;
        AdLoader builder = new AdLoader.Builder(activity, activity.getString (R.string.ad_unit_id)).forUnifiedNativeAd(
                new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
                    @Override
                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
                        // A native ad loaded successfully, check if the ad loader has finished loading
                        // and if so, insert the ads into the list.
                        counter++;
                        if(sizeAds > 1){
                            nativeAdList.add(unifiedNativeAd);
                            if(counter == sizeAds){
                                iGetAds.onGetSuccessAds(nativeAdList);
                            }
                        }else{

                            List<UnifiedNativeAd> nativeAdList = new ArrayList<>();
                            nativeAdList.add(unifiedNativeAd);
                            if(counter == sizeAds){
                                iGetAds.onGetSuccessAds(nativeAdList);
                            }
                        }
                    }
                }).withAdListener(
                new AdListener() {
                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        // A native ad failed to load, check if the ad loader has finished loading
                        // and if so, insert the ads into the list.
                        Log.e("MainActivity", "The previous native ad failed to load. Attempting to"
                                + " load another.");

                        iGetAds.onError();

                    }
                }).build();


        builder.loadAds(new AdRequest.Builder().build(),sizeAds);


    }


    public void populateNativeAdViewBig(UnifiedNativeAd nativeAd,
                                        UnifiedNativeAdView adView) {
        // Some assets are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        ((TextView) adView.getBodyView()).setText(nativeAd.getBody());
        ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());

        NativeAd.Image icon = nativeAd.getIcon();


        if (icon == null) {
            adView.getIconView().setVisibility(View.INVISIBLE);
        } else {
            ((ImageView) adView.getIconView()).setImageDrawable(icon.getDrawable());
            adView.getIconView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getPrice() == null) {
            adView.getPriceView().setVisibility(View.INVISIBLE);
        } else {
            adView.getPriceView().setVisibility(View.VISIBLE);
            ((TextView) adView.getPriceView()).setText(nativeAd.getPrice());
        }

        if (nativeAd.getStore() == null) {
            adView.getStoreView().setVisibility(View.INVISIBLE);
        } else {
            adView.getStoreView().setVisibility(View.VISIBLE);
            ((TextView) adView.getStoreView()).setText(nativeAd.getStore());
        }

        if (nativeAd.getStarRating() == null) {
            adView.getStarRatingView().setVisibility(View.INVISIBLE);
        } else {
            ((RatingBar) adView.getStarRatingView())
                    .setRating(nativeAd.getStarRating().floatValue());
            adView.getStarRatingView().setVisibility(View.VISIBLE);
        }

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        adView.setOnHierarchyChangeListener(new ViewGroup.OnHierarchyChangeListener() {
            @Override
            public void onChildViewAdded(View parent, View child) {
                if (child instanceof ImageView) {
                    ImageView imageView = (ImageView) child;
                    imageView.setAdjustViewBounds(true);
                }
            }

            @Override
            public void onChildViewRemoved(View parent, View child) {}
        });

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAd);
    }

    public void populateNativeAdViewSmall(UnifiedNativeAd nativeAd,
                                        UnifiedNativeAdView adView) {
        // Some assets are guaranteed to be in every UnifiedNativeAd.
        ((TextView) adView.getHeadlineView()).setText(nativeAd.getHeadline());
        ((Button) adView.getCallToActionView()).setText(nativeAd.getCallToAction());

        if (nativeAd.getAdvertiser() == null) {
            adView.getAdvertiserView().setVisibility(View.INVISIBLE);
        } else {
            ((TextView) adView.getAdvertiserView()).setText(nativeAd.getAdvertiser());
            adView.getAdvertiserView().setVisibility(View.VISIBLE);
        }

        LinearLayout containerAds =  adView.findViewById(R.id.container_ads_small);
        RelativeLayout shimmerContainer = adView.findViewById(R.id.container_ads_small_placeholder);
        containerAds.setVisibility(View.VISIBLE);
        shimmerContainer.setVisibility(View.GONE);


        adView.setOnHierarchyChangeListener(new ViewGroup.OnHierarchyChangeListener() {
            @Override
            public void onChildViewAdded(View parent, View child) {
                if (child instanceof ImageView) {
                    ImageView imageView = (ImageView) child;
                    imageView.setAdjustViewBounds(true);
                }
            }

            @Override
            public void onChildViewRemoved(View parent, View child) {}
        });

        // Assign native ad object to the native view.
        adView.setNativeAd(nativeAd);
    }

    public void placeHolderAdsViewSmall( UnifiedNativeAdView adView){
        LinearLayout containerAds =  adView.findViewById(R.id.container_ads_small);
        RelativeLayout shimmerContainer = adView.findViewById(R.id.container_ads_small_placeholder);
        containerAds.setVisibility(View.GONE);
        shimmerContainer.setVisibility(View.VISIBLE);
       // shimmerContainer.startShimmerAnimation();

    }



}
