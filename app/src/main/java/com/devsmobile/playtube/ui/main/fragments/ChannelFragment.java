package com.devsmobile.playtube.ui.main.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.devsmobile.playtube.R;
import com.devsmobile.playtube.data.adapter.VideoChannelRecyclerAdapter;
import com.devsmobile.playtube.data.model.Main;
import com.devsmobile.playtube.data.remote.VideoRequest;
import com.devsmobile.playtube.inyection.callback.IDataVideo;
import com.devsmobile.playtube.inyection.callback.IPlayVideo;
import com.devsmobile.playtube.ui.main.activities.PlayVideoActivity;
import com.devsmobile.playtube.util.HelperData;
import com.devsmobile.playtube.util.SnackBar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class ChannelFragment extends Fragment {


    private RecyclerView recyclerView;
    private String pageToken;
    private List<Object> currentDataVideos;
    private int sizeItemData;
    private boolean getMoreData;
    private boolean isGettingData;
    private VideoChannelRecyclerAdapter adapter;
    private NestedScrollView nestedScrollView;
    private ProgressBar progressBarBottom;
    private String channelId;
    private FirebaseAnalytics firebaseAnalytics;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup parent, Bundle state) {
        super.onCreateView(inflater, parent, state);

        View view = inflater.inflate(
                R.layout.fragmet_channel, parent, false);


        ImageView imgBanner = view.findViewById(R.id.img_banner);
        RoundedImageView imgAvatar = view.findViewById(R.id.img_avatar);
        TextView titleChannel = view.findViewById(R.id.tv_title_channel);
        TextView viewCount = view.findViewById(R.id.tv_views);
        TextView videoCount = view.findViewById(R.id.tv_video);
        TextView subscriberCount = view.findViewById(R.id.tv_subscriber);
        ImageButton btnClose = view.findViewById(R.id.btn_close_channel);

        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        //StatusBarUtil.setColor(getActivity(), getResources().getColor(android.R.color.transparent));

        this.progressBarBottom = view.findViewById(R.id.progress_bar_bottom);
        this.nestedScrollView = view.findViewById(R.id.content_channel);
        recyclerView = view.findViewById(R.id.rv_list_video);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        statusBarColor(nestedScrollView);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String channelUrl = bundle.getString("CHANNEL_URL", null);
            String channelTitle = bundle.getString("CHANNEL_TITLE", null);
            String channelAvatar = bundle.getString("CHANNEL_AVATAR", null);
            channelId = bundle.getString("CHANNEL_ID", null);
            String channelProfileColor = bundle.getString("CHANNEL_PROFILE_COLOR", "#000");
            long channelSubscriberCount = bundle.getLong("CHANNEL_SUBSCRIBER_COUNT", 0);
            long channelViewCount = bundle.getLong("CHANNEL_VIEWS_COUNT", 0);
            long channelVideoCount = bundle.getLong("CHANNEL_VIDEO_COUNT", 0);


            ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor(channelProfileColor));
            Picasso.get().load(channelUrl).placeholder(colorDrawable).into(imgBanner);
            Picasso.get().load(channelAvatar).into(imgAvatar);
            titleChannel.setText(channelTitle);

            viewCount.setText(new HelperData().getNumberFormatter(channelViewCount));
            videoCount.setText(new HelperData().getNumberFormatter(channelVideoCount));
            subscriberCount.setText(new HelperData().getNumberFormatter(channelSubscriberCount));

            getChannelVideoData(channelId);

            Bundle params = new Bundle();
            params.putString("channel_id", channelId);
            firebaseAnalytics.logEvent("view_channel", params);

        }

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right)
                        .remove(ChannelFragment.this).commit();

                getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
            }
        });

        nestedScrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = (View) nestedScrollView.getChildAt(nestedScrollView.getChildCount() - 1);

                int diff = (view.getBottom() - (nestedScrollView.getHeight() + nestedScrollView
                        .getScrollY()));

                if (diff == 0) {


                    progressBarBottom.setVisibility(View.VISIBLE);
                    //  if(progressBarBottom.getVisibility() == View.VISIBLE)
                    //    recyclerView.scrollToPosition(sizeItemData -1 );
                    if (progressBarBottom.getVisibility() == View.VISIBLE)
                        recyclerView.scrollToPosition(sizeItemData - 1);

                    getMoreData = true;
                    if (!isGettingData) {
                        Bundle params = new Bundle();
                        params.putString("get_more_data_channel", channelId);
                        firebaseAnalytics.logEvent("get_more_data_channel", params);

                        isGettingData = true;
                        getChannelVideoData(channelId);

                        //
                    }
                }
            }
        });


        return view;
    }


    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right)
                                .remove(ChannelFragment.this).commit();

                        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                        return true;
                    }

                }
                return false;
            }
        });
    }

    private void getChannelVideoData(String channelID) {


        showProgress();

        VideoRequest videoRequest = new VideoRequest(new IDataVideo() {
            @Override
            public void onGetDataVideoSuccess(Main main) {
                pageToken = main.getNextPageToken();

                if (currentDataVideos == null) {
                    currentDataVideos = new ArrayList<>(main.getItems());
                    sizeItemData = 0;
                }
                sizeItemData += main.getItems().size();

                if (getMoreData) {
                    currentDataVideos.addAll(main.getItems());
                    adapter.updateRecycler(currentDataVideos);

                } else {
                    List<Object> objectList = new ArrayList<>(currentDataVideos);
                    populateRecyclerView(objectList);
                }

                isGettingData = false;
                hideProgress();


            }

            @Override
            public void onGetDataVideoFailure(Throwable t) {
                SnackBar.errorData(getActivity());
            }
        });
        videoRequest.getChannelVideoData(channelID, pageToken);
    }

    private void populateRecyclerView(List<Object> arrayListItem) {
        adapter = new VideoChannelRecyclerAdapter(this.getActivity(), arrayListItem, new IPlayVideo() {
            @Override
            public void onPlayVideo(Bundle bundle) {
                Intent intent = new Intent(getActivity(), PlayVideoActivity.class);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);

            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void statusBarColor(NestedScrollView contentChannel) {
        contentChannel.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                View view = contentChannel.getChildAt(contentChannel.getChildCount() - 1);

                if (contentChannel.getScrollY() <= 255) {
                    assert getActivity() != null;
                    getActivity().getWindow().setStatusBarColor(Color.argb(contentChannel.getScrollY(), 0, 0, 0));
                } else {
                    assert getActivity() != null;
                    getActivity().getWindow().setStatusBarColor(Color.argb(255, 0, 0, 0));
                }
            }
        });
    }

    private void showProgress() {
        progressBarBottom.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        progressBarBottom.setVisibility(View.GONE);

    }
}