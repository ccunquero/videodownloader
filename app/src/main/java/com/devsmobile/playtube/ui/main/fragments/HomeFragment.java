package com.devsmobile.playtube.ui.main.fragments;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.devsmobile.playtube.R;
import com.devsmobile.playtube.data.adapter.ListRecyclerAdapter;
import com.devsmobile.playtube.data.adapter.VideoMostPopularRecyclerAdapter;
import com.devsmobile.playtube.data.local.SQLiteDatabaseHandler;
import com.devsmobile.playtube.data.model.DetailPlayList;
import com.devsmobile.playtube.data.model.Download;
import com.devsmobile.playtube.data.model.Item;
import com.devsmobile.playtube.data.model.Keys;
import com.devsmobile.playtube.data.model.Main;
import com.devsmobile.playtube.data.model.PlayList;
import com.devsmobile.playtube.data.model.Video;
import com.devsmobile.playtube.data.model.VideoFormat;
import com.devsmobile.playtube.data.remote.VideoRequest;
import com.devsmobile.playtube.inyection.callback.IClick;
import com.devsmobile.playtube.inyection.callback.IDataKeys;
import com.devsmobile.playtube.inyection.callback.IDataVideo;
import com.devsmobile.playtube.inyection.callback.IPlayVideo;
import com.devsmobile.playtube.inyection.callback.ISelectItem;
import com.devsmobile.playtube.ui.main.Common;
import com.devsmobile.playtube.ui.main.activities.PlayVideoActivity;
import com.devsmobile.playtube.util.Global;
import com.devsmobile.playtube.util.HelperData;
import com.devsmobile.playtube.util.Image;
import com.devsmobile.playtube.util.SnackBar;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


public class HomeFragment extends Fragment {
    private RecyclerView recyclerView;
    private SQLiteDatabaseHandler db;
    private LinearLayout containerCategory;
    private int currentIdCategory;
    private RelativeLayout layoutProgress;
    private ProgressBar progressBarBottom, progressBar;
    private LinearLayout containerEmpty;
    private LottieAnimationView lottieAnimationView;
    private TextView tvDescriptionAnimation;
    private Button btnTryAgain;
    private PlayList playListSelected;
    private String pageToken;
    private boolean getMoreData = false;
    private boolean isGettingData = false;
    private VideoMostPopularRecyclerAdapter recyclerAdapter;
    private List<Object> currentDataVideos;
    private int sizeItemData;
    private FirebaseAnalytics firebaseAnalytics;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(
                R.layout.fragment_home, container, false);

        db = new SQLiteDatabaseHandler(getActivity());

        recyclerView = rootView.findViewById(R.id.rv_list_video);
        layoutProgress = rootView.findViewById(R.id.layout_progress);
        progressBar = rootView.findViewById(R.id.progress_bar);
        progressBarBottom = rootView.findViewById(R.id.progress_bar_bottom);
        progressBar = rootView.findViewById(R.id.progress_bar);
        containerEmpty = rootView.findViewById(R.id.container_empty);
        btnTryAgain = rootView.findViewById(R.id.btnTryAgain);
        lottieAnimationView = rootView.findViewById(R.id.animation_view);
        tvDescriptionAnimation = rootView.findViewById(R.id.tv_description_animation);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        containerCategory = rootView.findViewById(R.id.container_category);
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
        hideProgress();
        getKeys("",true,container);//Obtiene los videoas mas populares por region
        btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                containerEmpty.setVisibility(View.GONE);
                btnTryAgain.setVisibility(View.GONE);
                getMostPopularVideo("", true);
                getCategoriesVideo(container);
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView rv, int dx, int dy) {

                if (!recyclerView.canScrollVertically(1)) {
                    progressBarBottom.setVisibility(View.VISIBLE);
                    if (progressBarBottom.getVisibility() == View.VISIBLE)
                        recyclerView.scrollToPosition(sizeItemData - 1);

                    getMoreData = true;
                    if (!isGettingData) {
                        isGettingData = true;
                        Bundle params = new Bundle();
                        params.putString("size_total_data", String.valueOf(sizeItemData));
                        firebaseAnalytics.logEvent("get_more_data_home", params);
                        getMostPopularVideo(String.valueOf(currentIdCategory), false);
                    }
                } else
                    progressBarBottom.setVisibility(View.GONE);

            }
        });


        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();


    }
    private void getKeys(String idCategory, boolean showProgress, ViewGroup container){
        VideoRequest videoRequest = new VideoRequest(new IDataKeys() {
            @Override
            public void onGetDataKeysSuccess(Keys main) {
                Global.API_KEYS =  main.getKeys().split(",");
                getMostPopularVideo(idCategory, showProgress);
                getCategoriesVideo(container);
            }
            @Override
            public void onGetDataKeysFailure(Throwable t) {
                SnackBar.errorData(getActivity());
            }
        });
        videoRequest.getKeys();
    }

    private void getMostPopularVideo(String idCategory, boolean showProgress) {


        if (currentDataVideos == null || getMoreData) {
            if (showProgress)
                showProgress();
            containerEmpty.setVisibility(View.GONE);
            VideoRequest videoRequest = new VideoRequest(new IDataVideo() {
                @Override
                public void onGetDataVideoSuccess(Main main) {


                    if (main.getItems().size() > 0) {
                        pageToken = main.getNextPageToken();
                        recyclerView.setVisibility(View.VISIBLE);

                        if (currentDataVideos == null) {
                            currentDataVideos = new ArrayList<>(main.getItems());

                            sizeItemData = 0;
                        }
                        sizeItemData += main.getItems().size();
                        if (getMoreData) {
                            currentDataVideos.addAll(main.getItems());
                            recyclerAdapter.updateRecycler(currentDataVideos);

                        } else {
                            List<Object> objectList = new ArrayList<>(currentDataVideos);
                            populateRecyclerView(objectList);
                        }

                    } else {
                        recyclerView.setVisibility(View.GONE);
                        containerEmpty.setVisibility(View.VISIBLE);
                        btnTryAgain.setVisibility(View.GONE);
                        lottieAnimationView.setAnimation(getResources().getIdentifier("empty", "raw", getActivity().getPackageName()));
                        tvDescriptionAnimation.setText(getResources().getString(R.string.video_no_found));
                        lottieAnimationView.playAnimation();
                    }
                    hideProgress();
                    isGettingData = false;
                    getMoreData = false;

                }

                @Override
                public void onGetDataVideoFailure(Throwable t) {
                    containerEmpty.setVisibility(View.VISIBLE);
                    btnTryAgain.setVisibility(View.VISIBLE);
                    isGettingData = false;
                    hideProgress();
                    try {
                        if (t instanceof IOException) {
                            tvDescriptionAnimation.setText(getResources().getString(R.string.error_network));
                            assert getActivity() != null;
                            lottieAnimationView.setAnimation(getResources().getIdentifier("network",
                                    "raw", getActivity().getPackageName()));
                        } else {
                            tvDescriptionAnimation.setText(getResources().getString(R.string.error_general));
                            assert getActivity() != null;
                            lottieAnimationView.setAnimation(getResources().getIdentifier("error_loading",
                                    "raw", getActivity().getPackageName()));
                            lottieAnimationView.setRepeatCount(2);
                        }
                        lottieAnimationView.playAnimation();

                    } catch (NullPointerException ex) {
                        SnackBar.errorData(getActivity());
                        containerEmpty.setVisibility(View.GONE);

                    }
                }
            });
            videoRequest.getMostPopularVideoData(idCategory, pageToken);
        } else {

            containerEmpty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            List<Object> objectList = new ArrayList<>(currentDataVideos);
            populateRecyclerView(objectList);

        }

    }


    private void getCategoriesVideo(ViewGroup container) {
        if (Global.CATEGORIES_VIDEO_DATA == null) {
            VideoRequest videoRequest = new VideoRequest(new IDataVideo() {
                @Override
                public void onGetDataVideoSuccess(Main main) {

                    List<Item> availableCategories = new ArrayList<>();
                    for (Item item :
                            main.getItems()) {
                        if (item.getSnippet().getAssignable().equals("true")) {
                            availableCategories.add(item);
                        }
                    }


                    Global.CATEGORIES_VIDEO_DATA = availableCategories;
                    Collections.shuffle(availableCategories);

                    populateCategories(availableCategories, container);
                }

                @Override
                public void onGetDataVideoFailure(Throwable t) {

                    SnackBar.errorData(getActivity());
                }
            });
            videoRequest.getCategories();
        } else {
            Collections.shuffle(Global.CATEGORIES_VIDEO_DATA);
            populateCategories(Global.CATEGORIES_VIDEO_DATA, container);
        }


    }

    private void populateCategories(List<Item> itemList, ViewGroup root) {

        containerCategory.removeAllViews();
        View view1 = getLayoutInflater().inflate(R.layout.item_category, root, false);
        LinearLayout linearLayout1 = view1.findViewById(R.id.layout_category);

        TextView tvTitle1 = view1.findViewById(R.id.tv_title);
        tvTitle1.setText(getResources().getString(R.string.popular));

        this.currentIdCategory = 0;
        linearLayout1.setId(this.currentIdCategory);

        assert getActivity() != null;
        linearLayout1.setBackground(getActivity().getResources().getDrawable(R.drawable.item_active));

        tvTitle1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle params = new Bundle();
                params.putString("title_category", tvTitle1.getText().toString());
                firebaseAnalytics.logEvent("change_category", params);

                LinearLayout container = containerCategory.findViewById(currentIdCategory);
                assert getActivity() != null;
                container.setBackground(getActivity().getResources().getDrawable(R.drawable.item_no_active));
                linearLayout1.setBackground(getActivity().getResources().getDrawable(R.drawable.item_active));
                currentIdCategory = linearLayout1.getId();

                currentDataVideos = null;
                getMostPopularVideo("", true);


            }
        });

        containerCategory.addView(view1);


        for (int i = 0; i < itemList.size(); i++) {

            Item currentItem = itemList.get(i);

            View view = getLayoutInflater().inflate(R.layout.item_category, root, false);
            LinearLayout linearLayout = view.findViewById(R.id.layout_category);

            TextView tvTitle = view.findViewById(R.id.tv_title);
            tvTitle.setText(currentItem.getSnippet().getTitle());
            linearLayout.setId(Integer.parseInt((String) currentItem.getId()));


            tvTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    LinearLayout linearLayout1 = containerCategory.findViewById(currentIdCategory);
                    linearLayout1.setBackground(getActivity().getResources().getDrawable(R.drawable.item_no_active));
                    linearLayout.setBackground(getActivity().getResources().getDrawable(R.drawable.item_active));
                    currentIdCategory = linearLayout.getId();
                    currentDataVideos = null;
                    getMostPopularVideo(String.valueOf(currentIdCategory), true);
                }
            });

            containerCategory.addView(view);

        }

    }

    private void populateRecyclerView(List<Object> items) {
        recyclerAdapter = new VideoMostPopularRecyclerAdapter(getActivity(), items, bundle -> {
            ChannelFragment dialog = new ChannelFragment();
            dialog.setArguments(bundle);
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right);
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.add(android.R.id.content, dialog);
            fragmentTransaction.commit();

        }, new IPlayVideo() {
            @Override
            public void onPlayVideo(Bundle bundle) {

                Bundle params = new Bundle();
                params.putString("play_video_home", "");
                firebaseAnalytics.logEvent("play_video_home", params);

                Intent intent = new Intent(getActivity(), PlayVideoActivity.class);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);

            }
        }, new ISelectItem() {
            @Override
            public void onSelectedItem(Object item, int index) {
                if (index == 0) { // Agregar a playlsit
                    SQLiteDatabaseHandler databaseHandler = new SQLiteDatabaseHandler(getActivity());
                    if (databaseHandler.getPlayLists().size() > 0) {
                        showDialogSelectPlayList((Item) item);
                    } else {
                        showDialogAddPlayList((Item) item);
                    }


                } else if (index == 1) { // Agregar a favoritos
                    long response = addVideo((Item) item, Global.TYPE_VIDEO_FAVORITE);
                    if (response > 0) {
                        SnackBar.success(getActivity(), getString(R.string.video_add_favorite));
                        Bundle params = new Bundle();
                        params.putString("title_video", ((Item) item).getSnippet().getTitle());
                        firebaseAnalytics.logEvent("add_to_favorites", params);
                    } else {
                        SnackBar.error(getActivity(), getString(R.string.error_add_video_favorite));
                        Bundle params = new Bundle();
                        params.putString("title_video", ((Item) item).getSnippet().getTitle());
                        firebaseAnalytics.logEvent("error_favorites_to_favorites", params);
                    }

                } else if (index == 2) { //Agregar en ver mas tarde
                    long response = addVideo((Item) item, Global.TYPE_VIDEO_SEE_LATER);
                    if (response > 0) {
                        SnackBar.success(getActivity(), getString(R.string.add_video_see_later));
                        Bundle params = new Bundle();
                        params.putString("title_video", ((Item) item).getSnippet().getTitle());
                        firebaseAnalytics.logEvent("add_to_see_later", params);
                    } else {
                        SnackBar.error(getActivity(), getString(R.string.error_add_video_see_later));
                        Bundle params = new Bundle();
                        params.putString("title_video", ((Item) item).getSnippet().getTitle());
                        firebaseAnalytics.logEvent("error_add_to_see_later", params);
                    }


                }


            }
        });


        recyclerView.setAdapter(recyclerAdapter);
    }


    private void showDialogSelectPlayList(Item item) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Black_NoTitleBar);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_play_list, null);


        Button btnAccept = dialogView.findViewById(R.id.btn_accept);
        Button btnNew = dialogView.findViewById(R.id.btn_new);
        ImageButton btnCancel = dialogView.findViewById(R.id.btn_close);


        dialogBuilder.setView(dialogView);

        RecyclerView rvPlayList = dialogView.findViewById(R.id.rv_play_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        rvPlayList.setLayoutManager(linearLayoutManager);

        SQLiteDatabaseHandler sqLiteDatabaseHandler = new SQLiteDatabaseHandler(getContext());
        List<PlayList> playLists = sqLiteDatabaseHandler.getPlayLists();
        ListRecyclerAdapter listRecyclerAdapter = new ListRecyclerAdapter(new ISelectItem() {
            @Override
            public void onSelectedItem(Object item, int index) {

                playListSelected = (PlayList) item;

            }
        }, playLists);

        rvPlayList.setAdapter(listRecyclerAdapter);

        AlertDialog alertDialog = dialogBuilder.create();

        if (alertDialog.getWindow() != null)
            alertDialog.getWindow().getAttributes().windowAnimations = R.style.SlidingDialogAnimation;


        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        layoutParams.copyFrom(alertDialog.getWindow().getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;

        alertDialog.show();
        alertDialog.getWindow().setGravity(Gravity.CENTER);
        alertDialog.getWindow().setAttributes(layoutParams);


        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (playListSelected != null) {
                    long response = addDetailList(item);
                    if (response > 0) {

                        SnackBar.success(getActivity(), getString(R.string.add_video_playlist));
                        alertDialog.hide();
                        Bundle params = new Bundle();
                        params.putString("title_video", item.getSnippet().getTitle());
                        firebaseAnalytics.logEvent("add_to_play_list", params);

                    } else
                        SnackBar.error(getActivity(), getString(R.string.error_add_video_playlist));
                    Bundle params = new Bundle();
                    params.putString("title_video", item.getSnippet().getTitle());
                    firebaseAnalytics.logEvent("error_add_to_play_list", params);
                } else {
                    SnackBar.info(getActivity(), getString(R.string.select_option));
                }
            }
        });
        btnNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.hide();
                showDialogAddPlayList(item);


            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.hide();
            }
        });

    }

    private void showDialogAddPlayList(Item item) {

        new Common(getActivity()).showDialogCreatePlayList(getActivity(), new IClick() {
            @Override
            public void onClickListener(Object object) {
                long responseAddPlayList = (long) object;
                if (responseAddPlayList > 0) {
                    playListSelected = new PlayList();
                    playListSelected.setIdPlayList((int) responseAddPlayList);
                    long responseAddDetailPlayList = addDetailList(item);
                    if (responseAddDetailPlayList > 0) {
                        SnackBar.success(getActivity(), getString(R.string.add_video_playlist));
                        Bundle params = new Bundle();
                        params.putString("title_video", item.getSnippet().getTitle());
                        firebaseAnalytics.logEvent("add_to_play_list", params);
                    } else {
                        SnackBar.error(getActivity(), getString(R.string.error_add_video_playlist));
                        Bundle params = new Bundle();
                        params.putString("title_video", item.getSnippet().getTitle());
                        firebaseAnalytics.logEvent("error_add_to_play_list", params);
                    }
                } else {
                    SnackBar.error(getActivity(), getResources().getString(R.string.play_list_created_error));
                }
            }
        });

    }

    private LinearLayout createRadioButton(ArrayList<com.devsmobile.playtube.data.model.VideoFormat> videoFormats) {
        LinearLayout container = new LinearLayout(getActivity());
        container.setOrientation(LinearLayout.VERTICAL);
        container.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        for (int i = 0; i < videoFormats.size(); i++) {
            VideoFormat videoFormat = videoFormats.get(i);

            LinearLayout option = (LinearLayout) getLayoutInflater().inflate(R.layout.radio_option_download, null, false);
            RadioButton radio = option.findViewById(R.id.radio_option);
            TextView tvDescription = option.findViewById(R.id.tv_description);
            ImageView imageVideo = option.findViewById(R.id.image_video);
            ImageView imageNoVideo = option.findViewById(R.id.image_no_video);
            ImageView imageAudio = option.findViewById(R.id.image_audio);
            ImageView imageNoAudio = option.findViewById(R.id.image_no_audio);

            StringBuilder description = new StringBuilder();
            boolean isOnlyAudio = false;
            if (videoFormat.getFormat().contains("audio only")) {
                description.append(getString(R.string.audio_only));
                description.append(videoFormat.getAbr() + "KBit/s ");
                description.append(videoFormat.getExtension());
                imageAudio.setVisibility(View.VISIBLE);
                imageNoVideo.setVisibility(View.VISIBLE);
                isOnlyAudio = true;
            } else if (videoFormat.getFormat().contains("video only") || videoFormat.getAbr() == 0) {
                description.append(getString(R.string.video_only));
                imageNoAudio.setVisibility(View.VISIBLE);
                imageVideo.setVisibility(View.VISIBLE);
            } else {
                description.append(getString(R.string.video_audio));
                imageAudio.setVisibility(View.VISIBLE);
                imageVideo.setVisibility(View.VISIBLE);
            }
            if (!isOnlyAudio) {
                description.append(videoFormat.getFormatNote() + " " + videoFormat.getExtension());
            }

            tvDescription.setText(description);
            option.setOnClickListener(view -> {
                clickItem(container, radio);

            });
            radio.setOnClickListener(view -> {
                clickItem(container, radio);


            });
            container.addView(option);

        }
        return container;

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 100) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // do something

            }

        }
    }


    private void clickItem(LinearLayout container, RadioButton radio) {

        for (int i = 0; i < container.getChildCount(); i++) {
            LinearLayout childAt = (LinearLayout) container.getChildAt(i);
            RadioButton radioButton = childAt.findViewById(R.id.radio_option);
            radioButton.setChecked(false);
        }
        radio.setChecked(true);

    }

    private boolean addDownload(String url, String title, String urlThumbnail) {
        boolean flag;
        try {
            Date date = new Date();
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            String format = formatter.format(date);

            Download download = new Download();
            download.setUrl(url);
            download.setTitleVideo(title);
            download.setDateDownload(format);
            download.setUrlThumbnail(urlThumbnail);
            download.setStateDownload(Global.DOWNLOAD_IN_HOLD);
            db.insert(download);
            flag = true;
        } catch (Exception ex) {
            flag = false;
        }

        return flag;
    }

    private long addDetailList(Item item) {
        SQLiteDatabaseHandler databaseHandler = new SQLiteDatabaseHandler(getActivity());
        DetailPlayList detailPlayList = new DetailPlayList();
        detailPlayList.setIdPlayList(playListSelected.getIdPlayList());
        detailPlayList.setChannelName(item.getSnippet().getChannelTitle());
        detailPlayList.setTittleVideo(item.getSnippet().getTitle());
        detailPlayList.setUrlThumbnail(item.getSnippet().getThumbnails().getMedium().getUrl());
        detailPlayList.setIdChannel(item.getSnippet().getChannelId());
        detailPlayList.setIdVideo(item.getId().toString());
        return databaseHandler.insertDetailPlayList(detailPlayList);
    }

    private long addVideo(Item item, String type) {

        SQLiteDatabaseHandler databaseHandler = new SQLiteDatabaseHandler(getContext());
        Video video = new Video();
        video.setIdVideoYouTube(item.getId().toString());
        video.setTitleVideo(item.getSnippet().getTitle());
        video.setChannelName(item.getSnippet().getChannelTitle());
        video.setUrlThumbnail(item.getSnippet().getThumbnails().getMedium().getUrl());
        video.setIdChannel(item.getSnippet().getChannelId());
        video.setType(type);
        return databaseHandler.insertVideo(video);
    }


    private void showProgress() {
        progressBar.setIndeterminate(true);
        layoutProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        layoutProgress.setVisibility(View.GONE);
        progressBarBottom.setVisibility(View.GONE);

    }


}
