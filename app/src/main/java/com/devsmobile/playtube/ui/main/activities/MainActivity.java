package com.devsmobile.playtube.ui.main.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import com.alfianyusufabdullah.SPager;
import com.devsmobile.playtube.R;
import com.devsmobile.playtube.data.services.TimerService;
import com.devsmobile.playtube.inyection.callback.IGetDownloads;
import com.devsmobile.playtube.inyection.callback.ILoading;
import com.devsmobile.playtube.inyection.callback.IOpenDialogChannel;
import com.devsmobile.playtube.inyection.callback.IProgress;
import com.devsmobile.playtube.inyection.callback.IProgressEmit;
import com.devsmobile.playtube.ui.main.fragments.CollectionListFragment;
import com.devsmobile.playtube.ui.main.fragments.HistoryFragment;
import com.devsmobile.playtube.ui.main.fragments.HomeFragment;
import com.devsmobile.playtube.ui.main.fragments.SearchVideoFragment;
import com.devsmobile.playtube.util.Global;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.mopub.common.MoPub;
import com.mopub.common.SdkConfiguration;
import com.mopub.common.SdkInitializationListener;
import com.mopub.mobileads.MoPubErrorCode;
import com.mopub.mobileads.MoPubView;
import com.vorlonsoft.android.rate.AppRate;
import com.vorlonsoft.android.rate.OnClickButtonListener;
import com.vorlonsoft.android.rate.StoreType;
import com.vorlonsoft.android.rate.Time;
import java.io.Serializable;

import static com.mopub.common.logging.MoPubLog.LogLevel.NONE;

public class MainActivity extends AppCompatActivity implements ILoading, IOpenDialogChannel, IProgress, Serializable  {


    private SPager sPager;
    private IProgressEmit progressEmit;
    //   private BubbleNavigationConstraintView navigationConstraintView;
    private FirebaseAnalytics firebaseAnalytics;
    private BottomNavigationView navigationView;
    private MoPubView moPubView;
    private LinearLayout containerMain ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);

        getWindow().setStatusBarColor(Color.TRANSPARENT);
        setTheme(R.style.StatusBar);
        sPager = findViewById(R.id.mainPage);
        sPager.initFragmentManager(getSupportFragmentManager());
        firebaseAnalytics = FirebaseAnalytics.getInstance(this);
        navigationView = findViewById(R.id.navigation_view);
        containerMain = findViewById(R.id.containerMain);

        sPager.addPages("Home", new HomeFragment());
        sPager.addPages("Search", new SearchVideoFragment());
        sPager.addPages("My videos", new CollectionListFragment());
        sPager.addPages("History", new HistoryFragment());


        sPager.build();
        sPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Bundle params = new Bundle();
                switch (position) {
                    case 0:
                        params.putString("home", "");
                        firebaseAnalytics.logEvent("select_page_home", params);
                        navigationView.setSelectedItemId(R.id.item_home);
                        break;
                    case 1:
                        params.putString("search", "");
                        firebaseAnalytics.logEvent("select_page_search", params);
                        navigationView.setSelectedItemId(R.id.item_search);
                        break;
                    case 2:
                        params.putString("my_videos", "");
                        firebaseAnalytics.logEvent("select_page_my_videos", params);
                        navigationView.setSelectedItemId(R.id.item_videos);
                        break;
                    case 3:
                        params.putString("history", "");
                        firebaseAnalytics.logEvent("select_page_history", params);
                        navigationView.setSelectedItemId(R.id.item_history);
                        break;
                }


            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        navigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item_home:
                        sPager.setCurrentItem(0);
                        break;
                    case R.id.item_search:
                        sPager.setCurrentItem(1);
                        break;
                    case R.id.item_videos:
                        sPager.setCurrentItem(2);
                        break;
                    case R.id.item_history:
                        sPager.setCurrentItem(3);
                        break;
                }

                return true;
            }
        });

        if(Global.PUBLISH_TO_PLAY_STORE)
            ratingApp();

        final SdkConfiguration.Builder configBuilder = new SdkConfiguration.Builder("b195f8dd8ded45fe847ad89ed1d016da");

        configBuilder.withLogLevel(NONE);
        MoPub.initializeSdk(this, configBuilder.build(), new SdkInitializationListener() {
            @Override
            public void onInitializationFinished() {
                System.out.println("initialized");
            }
        });
        moPubView = (MoPubView) findViewById(R.id.mopub_view);
        moPubView.setAdUnitId(Global.APP_ID_MOPUB);
        moPubView.loadAd();
        moPubView.setBannerAdListener(new MoPubView.BannerAdListener() {
            @Override
            public void onBannerLoaded(@NonNull MoPubView banner) {
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
                params.setMargins(0, 0, 0, 0);
                containerMain.setLayoutParams(params);
            }

            @Override
            public void onBannerFailed(MoPubView banner, MoPubErrorCode errorCode) {
                System.out.println("Faild");
            }

            @Override
            public void onBannerClicked(MoPubView banner) {
                System.out.println("CLicked");
            }

            @Override
            public void onBannerExpanded(MoPubView banner) {
                System.out.println("Expended");
            }

            @Override
            public void onBannerCollapsed(MoPubView banner) {
                System.out.println("Collapsed");
            }
        });

    }
    private void ratingApp() {
        AppRate.with(this)
                .setStoreType(StoreType.GOOGLEPLAY)
                .setTimeToWait(Time.DAY, (short) 10) // default is 10 days, 0 means install millisecond, 10 means app is launched 10 or more time units later than installation
                .setLaunchTimes((byte) 10)           // default is 10, 3 means app is launched 3 or more times
                .setRemindTimeToWait(Time.DAY, (short) 1) // default is 1 day, 1 means app is launched 1 or more time units after neutral button clicked
                .setRemindLaunchesNumber((byte) 0)  // default is 0, 1 means app is launched 1 or more times after neutral button clicked
                .setSelectedAppLaunches((byte) 1)   // default is 1, 1 means each launch, 2 means every 2nd launch, 3 means every 3rd launch, etc
                .setShowLaterButton(true)           // default is true, true means to show the Neutral button ("Remind me later").
                .set365DayPeriodMaxNumberDialogLaunchTimes((short) 3) // default is unlimited, 3 means 3 or less occurrences of the display of the Rate Dialog within a 365-day period
                .setVersionCodeCheck(false)          // default is false, true means to re-enable the Rate Dialog if a new version of app with different version code is installed
                .setVersionNameCheck(false)          // default is false, true means to re-enable the Rate Dialog if a new version of app with different version name is installed
                .setThemeResId(R.style.AlertRate)
                .setDebug(false)                    // default is false, true is for development only, true ensures that the Rate Dialog will be shown each time the app is launched
                .setOnClickButtonListener(new OnClickButtonListener() {
                    @Override
                    public void onClickButton(byte which) {
                        Bundle params = new Bundle();

                        switch (which) {
                            case -1:
                                params.putString("click_button_now", "-1");
                                firebaseAnalytics.logEvent("rate_now", params);
                                break;
                            case -2:
                                params.putString("click_button_not", "-2");
                                firebaseAnalytics.logEvent("rate_not_thanks", params);
                                break;
                            case -3:
                                params.putString("click_later", "-3");
                                firebaseAnalytics.logEvent("rate_remember_later", params);
                                break;

                        }

                    }
                })
                .monitor();

        if (AppRate.with(this).getStoreType() == StoreType.GOOGLEPLAY) { // Checks that current app store type from library options is StoreType.GOOGLEPLAY
            if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this) != ConnectionResult.SERVICE_MISSING) { // Checks that Google Play is available
                AppRate.showRateDialogIfMeetsConditions(this); // Shows the Rate Dialog when conditions are met
            }
        } else {
            AppRate.showRateDialogIfMeetsConditions(this);     // Shows the Rate Dialog when conditions are met
        }
    }


    @Override
    public void onInitLoading() {


    }

    @Override
    public void onFinishLoading() {

    }

    @Override
    public void onOpenDialogChannel(Bundle bundle) {

    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();

        } else {
            super.onBackPressed();
        }
    }


    @Override
    public void onInit() {
        if (this.progressEmit != null) {
            this.progressEmit.onInit();
        }
    }

    @Override
    public void onProgress(float progress, long eatSeconds) {
        if (this.progressEmit != null) {
            this.progressEmit.onProgress(progress, eatSeconds);
        }
    }

    @Override
    public void onFinish() {
        if (this.progressEmit != null) {
            this.progressEmit.onFinish();
        }
    }

    @Override
    public void onFailure() {
        if (this.progressEmit != null) {
            this.progressEmit.onFailure();
        }
    }

    public void setListenerAysncTask(IProgressEmit progressEmit) {
        this.progressEmit = progressEmit;
    }

    public void setListenerDownload(IGetDownloads getDownloads) {
    }

    public static class DownloadCancelReceiver extends BroadcastReceiver implements Serializable {

        @Override
        public void onReceive(Context context, Intent intent) {

            System.out.println("PLAY MUSIC CLICK !!!!!!!");
        }
    }



    public static class MessageHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message message) {

      /*          new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            TimeUnit.SECONDS.sleep(10);
                            if(adColony != null)
                                adColony.show();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }).run();*/


        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!Global.PUBLISH_TO_PLAY_STORE && Global.START_SERVICE_ADS) {

            Handler messageHandler = new MessageHandler();
            Intent startService = new Intent(MainActivity.this, TimerService.class);
            startService.putExtra("MESSENGER", new Messenger(messageHandler));
            startService(startService);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (!Global.PUBLISH_TO_PLAY_STORE && Global.START_SERVICE_ADS) {

            stopService(new Intent(this,
                    TimerService.class));

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (!Global.PUBLISH_TO_PLAY_STORE && Global.START_SERVICE_ADS) {

            stopService(new Intent(this,
                    TimerService.class));

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(moPubView != null)
            moPubView.destroy();


    }
}
