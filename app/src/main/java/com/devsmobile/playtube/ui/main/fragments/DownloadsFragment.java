package com.devsmobile.playtube.ui.main.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.abdularis.buttonprogress.DownloadButtonProgress;

import java.io.Serializable;

import com.devsmobile.playtube.data.adapter.VideoDownloadsRecyclerAdapter;
import com.devsmobile.playtube.data.local.SQLiteDatabaseHandler;
import com.devsmobile.playtube.inyection.callback.IGetDownloads;
import com.devsmobile.playtube.inyection.callback.IProgressEmit;
import com.devsmobile.playtube.R;
import com.devsmobile.playtube.ui.main.activities.MainActivity;
import com.devsmobile.playtube.util.Global;

public class DownloadsFragment extends Fragment implements Serializable {


    private SQLiteDatabaseHandler db;
    private RecyclerView recyclerView;
    private VideoDownloadsRecyclerAdapter.YoutubeViewHolder currentViewDownload;
    private ImageButton btnDelete;


    public DownloadsFragment() {

    }

    /*  public static DownloadsFragment newInstance(MainActivity activity){
          Bundle args = new Bundle();
          args.putSerializable("activity", (Serializable) activity);
          DownloadsFragment f = new DownloadsFragment();
          f.setArguments(args);
          return f;
      }*/
    public DownloadsFragment(MainActivity activity) {
        //  Bundle args = new Bundle();
        //    MainActivity activity = (MainActivity) args.getSerializable("activity");
        if (activity == null) return;

        activity.setListenerDownload(new IGetDownloads() {
            @Override
            public void getDownloads() {
                populateRecyclerView();
            }
        });
        activity.setListenerAysncTask(new IProgressEmit() {
            @Override
            public void onInit() {
                populateRecyclerView();
                if (currentViewDownload != null) {
                    TextView tvState = currentViewDownload.tvState;
                    DownloadButtonProgress buttonProgress = currentViewDownload.buttonProgress;
                    buttonProgress.setIndeterminate();
                    tvState.setText("Iniciando..");

                }
            }

            @Override
            public void onProgress(float progress, long eatSeconds) {
                if (currentViewDownload != null) {
                    DownloadButtonProgress buttonProgress = currentViewDownload.buttonProgress;
                    TextView tvProgress = currentViewDownload.tvProgress;
                    TextView tvState = currentViewDownload.tvState;
                    tvProgress.setVisibility(View.VISIBLE);
                    buttonProgress.setDeterminate();
                    buttonProgress.setCurrentProgress((int) progress);
                    tvProgress.setText(progress + "%");
                    tvState.setText("Descargando");

                }
            }

            @Override
            public void onFinish() {
                if (currentViewDownload != null) {
                    DownloadButtonProgress buttonProgress = currentViewDownload.buttonProgress;
                    buttonProgress.setFinish();
                    TextView tvState = currentViewDownload.tvState;
                    tvState.setText("Finalizado");
                    populateRecyclerView();

                }
            }

            @Override
            public void onFailure() {
                if (currentViewDownload != null) {
                    DownloadButtonProgress buttonProgress = currentViewDownload.buttonProgress;
                    buttonProgress.setIdle();
                    TextView tvState = currentViewDownload.tvState;
                    tvState.setText("Error al descargar");
                    populateRecyclerView();
                }
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_downloads, container, false);

        db = new SQLiteDatabaseHandler(getActivity());
        recyclerView = view.findViewById(R.id.rv_list);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);

        btnDelete = view.findViewById(R.id.btn_delete);

        btnDelete.setOnClickListener(view1 -> {
            Toast.makeText(getActivity(), "delteing ...", Toast.LENGTH_SHORT).show();
            db.deleteAllDownload();
            populateRecyclerView();
        });
        populateRecyclerView();
        //    updateRecycler();
        return view;
    }

    private void populateRecyclerView() {
        SharedPreferences prefs =
                getActivity().getSharedPreferences(Global.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);


        VideoDownloadsRecyclerAdapter videoDownloadsRecyclerAdapter =
                new VideoDownloadsRecyclerAdapter(getActivity(), db.getDownloads(), (urlVideo, view) -> {
                    String videoUrl = prefs.getString("currentVideoUrl", "");

                    if (videoUrl.length() > 0) {
                        if (videoUrl.equals(urlVideo)) {
                            currentViewDownload = view;
                        }
                    }
                });
    }


}
