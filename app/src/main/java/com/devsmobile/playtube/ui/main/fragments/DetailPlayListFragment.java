package com.devsmobile.playtube.ui.main.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Objects;

import com.devsmobile.playtube.data.adapter.PlayListRecyclerAdapter;
import com.devsmobile.playtube.data.local.SQLiteDatabaseHandler;
import com.devsmobile.playtube.inyection.callback.IPlayVideo;
import com.devsmobile.playtube.R;
import com.devsmobile.playtube.ui.main.activities.PlayVideoActivity;
import com.google.firebase.analytics.FirebaseAnalytics;


public class DetailPlayListFragment extends Fragment {
    private FirebaseAnalytics firebaseAnalytics;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        SQLiteDatabaseHandler databaseHandler = new SQLiteDatabaseHandler(getActivity());
        View view = inflater.inflate(R.layout.fragment_play_list, container, false);
        ImageButton btnBack = view.findViewById(R.id.btn_back);
        TextView tvTitle = view.findViewById(R.id.tv_title_play_list);
        RecyclerView rvPlayList = view.findViewById(R.id.rv_play_list);
        rvPlayList.setLayoutManager(new LinearLayoutManager(getContext()));
        Bundle bundle = getArguments();
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());



        if (bundle != null) {
            int idPlayList = bundle.getInt("ID_PLAY_LIST", 0);
            tvTitle.setText(bundle.getString("TITLE_PLAY_LIST", ""));

            Bundle params = new Bundle();
            params.putString("id_detail_playlist", String.valueOf(idPlayList) );
            firebaseAnalytics.logEvent("view_detail_playlist", params);

            PlayListRecyclerAdapter adapter = new PlayListRecyclerAdapter(getContext(),
                    new ArrayList<>(databaseHandler.getDetailPlayList(idPlayList)) , new IPlayVideo() {
                @Override
                public void onPlayVideo(Bundle bundle) {

                    Bundle params = new Bundle();
                    params.putString("pay_video", "" );
                    firebaseAnalytics.logEvent("play_video_from_detail_playlist", params);

                    Intent intent = new Intent(getActivity(), PlayVideoActivity.class);
                    bundle.putString("ID_PLAY_LIST", String.valueOf(idPlayList));
                    bundle.putString("TITLE_PLAY_LIST", tvTitle.getText().toString());
                    intent.putExtras(bundle);
                    intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    startActivity(intent);

                }
            }, null);

            rvPlayList.setAdapter(adapter);
        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right)
                        .remove(DetailPlayListFragment.this).commit();

            }
        });
        return view;

    }
    @Override
    public void onResume() {
        super.onResume();
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();

        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                                .setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right)
                                .remove(DetailPlayListFragment.this).commit();

                        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
                        return true;
                    }

                }
                return false;
            }
        });
    }


}
