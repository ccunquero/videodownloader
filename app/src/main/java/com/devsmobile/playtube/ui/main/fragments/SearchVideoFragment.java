package com.devsmobile.playtube.ui.main.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.arlib.floatingsearchview.FloatingSearchView;
import com.google.android.gms.ads.formats.UnifiedNativeAd;

import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent;
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener;

import java.util.ArrayList;
import java.util.List;

import com.devsmobile.playtube.data.adapter.VideoSearchRecyclerAdapter;
import com.devsmobile.playtube.data.model.Main;
import com.devsmobile.playtube.data.remote.VideoRequest;
import com.devsmobile.playtube.inyection.callback.IDataVideo;
import com.devsmobile.playtube.inyection.callback.IGetAds;
import com.devsmobile.playtube.inyection.callback.IOpenDialogChannel;
import com.devsmobile.playtube.inyection.callback.IOpenPlayList;
import com.devsmobile.playtube.inyection.callback.IPlayVideo;
import com.devsmobile.playtube.R;
import com.devsmobile.playtube.ui.main.Common;
import com.devsmobile.playtube.ui.main.activities.PlayVideoActivity;
import com.devsmobile.playtube.util.Global;
import com.devsmobile.playtube.util.SnackBar;
import com.google.firebase.analytics.FirebaseAnalytics;

public class SearchVideoFragment extends Fragment {

    private RecyclerView recyclerView;
    private String currentQuery;
    private String pageToken;
    private LinearLayout layoutInit;
    private int sizeNullValues = 0;
    private RelativeLayout layoutProgress;
    private FirebaseAnalytics firebaseAnalytics;
    private ProgressBar progressBarBottom;
    private boolean getMoreData = false;
    private boolean isGettingData = false;
    private int sizeItemData;
    private VideoSearchRecyclerAdapter adapter;

    private List<Object> currentDataVideos;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_search_video, container, false);


        layoutProgress = rootView.findViewById(R.id.layout_progress);
        progressBarBottom  = rootView.findViewById(R.id.progress_bar_bottom);
        layoutInit = rootView.findViewById(R.id.layout_init);
        FloatingSearchView floatingSearchView = rootView.findViewById(R.id.floating_search_view);
        recyclerView = rootView.findViewById(R.id.recycler_search);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);



        floatingSearchView.setOnQueryChangeListener((oldQuery
                , newQuery) -> {
            this.currentQuery = newQuery;
        });
        firebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());


        floatingSearchView.setOnMenuItemClickListener(item -> {
            if (item.getTitle().equals("Search")) {

                View view = getActivity().getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                isGettingData = true;
                if(this.currentQuery != null && !this.currentQuery.equals("null")){
                    getMoreData = false;
                    this.getQueryData(this.currentQuery, true, true);

                }
            }
        });

        KeyboardVisibilityEvent.setEventListener(
                getActivity(),
                new KeyboardVisibilityEventListener() {
                    @Override
                    public void onVisibilityChanged(boolean isOpen) {
                        if (!isOpen) {
                            if(!isGettingData){
                                if(currentQuery != null && !currentQuery.equals("null")){
                                    getMoreData = false;
                                    getQueryData(currentQuery, true, true);
                                }
                            }

                        }
                    }
                }
                );


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView rv, int dx, int dy) {

                if (!recyclerView.canScrollVertically(1)) {
                    progressBarBottom.setVisibility(View.VISIBLE);
                    if(progressBarBottom.getVisibility() == View.VISIBLE)
                        recyclerView.scrollToPosition(sizeItemData -1 );

                    getMoreData = true;
                    if(!isGettingData){
                        isGettingData = true;
                        Bundle params = new Bundle();
                        params.putString("size_total_data", String.valueOf(sizeItemData) );
                        firebaseAnalytics.logEvent("get_more_data_home", params);
                        if(currentQuery != null && !currentQuery.equals("null"))
                            getQueryData(currentQuery, false, false);
                    }
                }else
                    progressBarBottom.setVisibility(View.GONE);

            }
        });

        return rootView;
    }

    private void getQueryData(String query, boolean showProgress, boolean resetPageToken) {
        //handleAdsAdapter();
        Bundle params = new Bundle();
        params.putString("search_query", query );
        firebaseAnalytics.logEvent("search_query", params);

        if(resetPageToken){
            currentDataVideos = null;
            sizeItemData = 0;
            pageToken = null;
        }


        if(showProgress)
            showProgress();
        VideoRequest request = new VideoRequest(new IDataVideo() {
            @Override
            public void onGetDataVideoSuccess(Main main) {
                pageToken = main.getNextPageToken();
                if(currentDataVideos == null){
                    currentDataVideos = new ArrayList<>(main.getItems());
                    populateRecyclerView(currentDataVideos, recyclerView);
                    sizeItemData = 0;
                }else{
                    currentDataVideos.addAll( new ArrayList<>(main.getItems()));
                    adapter.updateRecycler(currentDataVideos);
                }
                sizeItemData += new ArrayList<>(main.getItems()).size();
                isGettingData = false;
                getMoreData = false;
                hideProgress();

            }

            @Override
            public void onGetDataVideoFailure(Throwable t) {
                isGettingData = false;
                getMoreData = false;
            }
        });

        VideoRequest videoRequest = new VideoRequest(new IDataVideo() {
            @Override
            public void onGetDataVideoSuccess(Main main) {
                    request.getDataVideoInfo(main,true);
            }
            @Override
            public void onGetDataVideoFailure(Throwable t) {
               hideProgress();
                SnackBar.errorData(getActivity());
                isGettingData = false;
            }
        });
        //Se busca los videos, canales o playList, el parametro idRelatedVideo se le pasa
        //como valor vacio porque no se necesita obtener los vidoes relacionados.
        //

        videoRequest.getQueryVideoData(query, Global.EMPTY_STRING, pageToken, Global.MAX_RESULT_SEARCH);
    }

    private void populateRecyclerView(List<Object> arrayListItem, RecyclerView recyclerView) {
        adapter = new VideoSearchRecyclerAdapter(getContext(), arrayListItem, new IPlayVideo() {
            @Override
            public void onPlayVideo(Bundle bundle) {

                Bundle params = new Bundle();
                params.putString("play_video", "" );
                firebaseAnalytics.logEvent("play_video_from_search", params);

                Intent intent = new Intent(getActivity(), PlayVideoActivity.class);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }
        }, new IOpenDialogChannel() {
            @Override
            public void onOpenDialogChannel(Bundle bundle) {

                Bundle params = new Bundle();
                params.putString("open_channel", "" );
                firebaseAnalytics.logEvent("open_channel_from_search", params);

                ChannelFragment dialog = new ChannelFragment();
                dialog.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.add(android.R.id.content, dialog);
                fragmentTransaction.commit();
            }
        }, new IOpenPlayList() {
            @Override
            public void onShowPlayList(Bundle bundle) {

                Bundle params = new Bundle();
                params.putString("open_playlist", "" );
                firebaseAnalytics.logEvent("open_playlist_from_search", params);

                PlayListFragment dialog = new PlayListFragment();
                dialog.setArguments(bundle);
                //       bundle.putSerializable("manager", (Serializable) getFragmentManager());
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_right);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.add(android.R.id.content, dialog);
                fragmentTransaction.commit();
            }
        });
        recyclerView.setAdapter(adapter);

        this.layoutInit.setVisibility(View.GONE);
        this.recyclerView.setVisibility(View.VISIBLE);
        this.closeKeyBoard();
    }

    private void closeKeyBoard() {
        final InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null && inputMethodManager.isActive()) {
            if (getActivity().getCurrentFocus() != null) {
                inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        }
    }


    //Agrega valores NULL al array unicamente para guardar la posicion de los anuncios
    private List<Object> addNullValues(List<Object> objectList){
        int counter = 0;
        sizeNullValues = 0;
        List<Object> newObjectList = new ArrayList<>();
        for (Object item : objectList) {
            newObjectList.add(item);
            if(sizeNullValues < 5){ // Se valida que sea menor que sinco porque solo se van a obtener como maximo 5 nuncios
                if(counter == 0 || counter == 3){
                    newObjectList.add(null); // Al inicio se agregan como NULL para despues reemplazarlos por anuncios
                    sizeNullValues++;
                    if(counter == 3)
                        counter = 0;
                }
                counter++;
            }
        }

        return newObjectList;
    }

    private void handleAdsAdapter(){
        new Common(getActivity()).getNativeAds(getActivity(), new IGetAds() {
            @Override
            public void onGetSuccessAds(List<UnifiedNativeAd> nativeAdList) {
                List<Object> objectList = new ArrayList<>();
                int positionAd = 0;
                if(currentDataVideos != null){
                    for (Object item:currentDataVideos) {
                        if(item == null && positionAd < nativeAdList.size() ){
                            objectList.add(nativeAdList.get(positionAd));
                            positionAd++;
                        }else{
                            objectList.add(item);
                        }
                    }
                    if(adapter != null){
                        currentDataVideos = objectList;
                        adapter.updateRecycler(currentDataVideos);
                    }
                }
                hideProgress();
            }

            @Override
            public void onError() {
                SnackBar.errorData(getActivity());
                hideProgress();

            }
        },(sizeNullValues == 0) ? 5 : sizeNullValues);
    }

    private void showProgress() {
        layoutProgress.setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        layoutProgress.setVisibility(View.GONE);
    }

}

