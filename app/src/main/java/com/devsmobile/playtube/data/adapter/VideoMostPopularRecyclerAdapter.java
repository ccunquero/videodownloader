package com.devsmobile.playtube.data.adapter;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.makeramen.roundedimageview.RoundedImageView;
import com.skydoves.powermenu.MenuAnimation;
import com.skydoves.powermenu.OnMenuItemClickListener;
import com.skydoves.powermenu.PowerMenu;
import com.skydoves.powermenu.PowerMenuItem;

import java.util.List;

import com.devsmobile.playtube.R;
import com.devsmobile.playtube.data.model.Item;
import com.devsmobile.playtube.inyection.callback.IOpenDialogChannel;
import com.devsmobile.playtube.inyection.callback.IPlayVideo;
import com.devsmobile.playtube.inyection.callback.ISelectItem;
import com.devsmobile.playtube.ui.main.Common;
import com.devsmobile.playtube.util.Global;
import com.devsmobile.playtube.util.HelperData;
import com.devsmobile.playtube.util.Image;

public class VideoMostPopularRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<Object> videoModelArrayList;
    private IOpenDialogChannel openDialogChannel;
    private IPlayVideo iPlayVideo;
    private ISelectItem iSelectItem;
    private PowerMenu powerMenu;




    public VideoMostPopularRecyclerAdapter(Context context, List<Object> videoModelArrayList,
                                           IOpenDialogChannel iOpenDialogChannel, IPlayVideo iPlayVideo, ISelectItem iSelectItem) {
        this.context = context;
        this.videoModelArrayList = videoModelArrayList;
        this.openDialogChannel = iOpenDialogChannel;
        this.iPlayVideo = iPlayVideo;
        this.iSelectItem = iSelectItem;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder  onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == Global.UNIFIED_NATIVE_AD_VIEW_TYPE) {
            LayoutInflater layoutInflaterUnified = LayoutInflater.from(parent.getContext());
            View viewUnified = layoutInflaterUnified.inflate(R.layout.ad_unifed_big, parent, false);
            return new UnifiedNativeAdViewHolder(viewUnified);
        }else{
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_video_most_popular_list, parent, false);
            return new YoutubeViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder  recyclerHolder, final int position) {
        int viewType = getItemViewType(position);
        System.out.println("POSITION VIEW HOLDER " + position);
        if(viewType == Global.UNIFIED_NATIVE_AD_VIEW_TYPE){
            UnifiedNativeAd nativeAd = (UnifiedNativeAd) videoModelArrayList.get(position);
            new Common(context).populateNativeAdViewBig(nativeAd, ((UnifiedNativeAdViewHolder) recyclerHolder).getAdView());
        }else{
            YoutubeViewHolder holder  = (YoutubeViewHolder) recyclerHolder;
            final Item itemModel = (Item) videoModelArrayList.get(position);
            long viewCont;
            try{
                viewCont = Long.parseLong(itemModel.getStatistics().getViewCount());
            }catch (Exception ex){viewCont = 0;}

            holder.tvTitle.setText(itemModel.getSnippet().getTitle());
            holder.tvChannel.setText(itemModel.getSnippet().getChannelTitle());
            holder.tvVisits.setText(new HelperData().getNumberFormatter(viewCont) + " " + context.getResources().getString(R.string.view));
            try {
                holder.tvTime.setText(new HelperData().durationFormatter(itemModel.getContentDetails().getDuration()));
            } catch (Exception ex) {
                System.out.println(ex.toString());
                holder.tvTime.setText(R.string.time_zero);
            }


            //   holder.tvTitle.setText(String.valueOf( holder.tvTime.getLineCount()));
            if (holder.tvTime.getLineCount() > 0) {
                //    holder.containerTime.setVisibility(View.GONE);
            }

            holder.imgAvatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Bundle bundle = new Bundle();
                    bundle.putString("CHANNEL_URL", itemModel.getSnippet().getUrlBanner());
                    bundle.putString("CHANNEL_TITLE", itemModel.getSnippet().getChannelTitle());
                    bundle.putString("CHANNEL_AVATAR", itemModel.getSnippet().getUrlAvatar());
                    bundle.putString("CHANNEL_PROFILE_COLOR", itemModel.getSnippet().getChannelProfileColor());
                    bundle.putLong("CHANNEL_VIEWS_COUNT", itemModel.getStatistics().getViewCountChannel() != null ?
                            Long.parseLong(itemModel.getStatistics().getViewCountChannel()) : 0);
                    bundle.putLong("CHANNEL_SUBSCRIBER_COUNT", itemModel.getStatistics().getSubscriberCount() != null ?
                            Long.parseLong(itemModel.getStatistics().getSubscriberCount()) : 0);
                    bundle.putLong("CHANNEL_VIDEO_COUNT", itemModel.getStatistics().getVideoCount() != null ?
                            Long.parseLong(itemModel.getStatistics().getVideoCount()) : 0);
                    bundle.putString("CHANNEL_ID", itemModel.getSnippet().getChannelId());

                    openDialogChannel.onOpenDialogChannel(bundle);
                }
            });

            holder.imgVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    bundle.putString("VIDEO_ID", itemModel.getId().toString());
                    bundle.putString("VIDEO_TITLE", itemModel.getSnippet().getTitle());
                    bundle.putLong("VIDEO_LIKE", itemModel.getStatistics().getLikeCount() != null ?
                            Long.parseLong(itemModel.getStatistics().getLikeCount()) : 0);
                    bundle.putLong("VIDEO_DISLIKE", itemModel.getStatistics().getDislikeCount() != null ?
                            Long.parseLong(itemModel.getStatistics().getDislikeCount()) : 0);
                    bundle.putLong("VIDEO_VIEW", itemModel.getStatistics().getViewCount() != null ?
                            Long.parseLong(itemModel.getStatistics().getViewCount()) : 0);
                    bundle.putString("VIDEO_DESCRIPTION", itemModel.getSnippet().getDescription());
                    bundle.putString("VIDEO_URL_THUMBNAIL", itemModel.getSnippet().getThumbnails().getDefault().getUrl());
                    bundle.putString("CHANNEL_ID", itemModel.getSnippet().getChannelId());
                    iPlayVideo.onPlayVideo(bundle);

                }
            });


            Image.loadImage(holder.imgVideo, itemModel.getSnippet().getThumbnails().getMedium().getUrl());
            Image.loadImage(holder.imgAvatar, itemModel.getSnippet().getUrlAvatar());

            holder.btnMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    //iDownload.onItemDownloadClick(itemModel);


                    powerMenu = new PowerMenu.Builder(context)

                            .addItem(new PowerMenuItem(context.getString(R.string.add_to_playlist), false))
                            .addItem(new PowerMenuItem(context.getString(R.string.add_to_favorites), false))
                            .addItem(new PowerMenuItem(context.getString(R.string.add_to_see_later), false))
                            .setAnimation(MenuAnimation.SHOWUP_TOP_RIGHT) // Animation start point (TOP | LEFT).
                            .setMenuRadius(10f) // sets the corner radius.
                            .setMenuShadow(10f) // sets the shadow.
                            .setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryText))
                            .setTextGravity(Gravity.START)
                            .setSelectedTextColor(Color.WHITE)
                            .setTextSize(15)
                            .setWidth(270)
                            .setMenuColor(ContextCompat.getColor(context, R.color.colorDark_2))
                            .setSelectedMenuColor(ContextCompat.getColor(context, R.color.colorAccent))
                            .setOnMenuItemClickListener(onMenuItemClickListener(itemModel))
                            .build();

                    powerMenu.showAsDropDown(v);
                }


            });
        }
    }

    private OnMenuItemClickListener<PowerMenuItem> onMenuItemClickListener(Item itemModel) {
        return new OnMenuItemClickListener<PowerMenuItem>() {
            @Override
            public void onItemClick(int position, PowerMenuItem item) {

                iSelectItem.onSelectedItem(itemModel, position);
                powerMenu.setSelectedPosition(position); // change selected item
                powerMenu.dismiss();

            }
        };
    }

    @Override
    public int getItemCount() {
        return videoModelArrayList != null ? videoModelArrayList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {

        Object recyclerViewItem = videoModelArrayList.get(position);
        if (recyclerViewItem instanceof UnifiedNativeAd) {
            return Global.UNIFIED_NATIVE_AD_VIEW_TYPE;
        }else{
            return Global.ITEM_VIEW_TYPE;
        }
    }



    public void updateRecycler(List<Object> data){
        this.videoModelArrayList.clear();
        this.videoModelArrayList.addAll(data);
        this.notifyDataSetChanged();

    }
     private class YoutubeViewHolder extends RecyclerView.ViewHolder {

        private RoundedImageView imgVideo, imgAvatar;
        private TextView tvTitle, tvVisits, tvChannel, tvTime;
        private ImageButton btnMore;


        private YoutubeViewHolder(View itemView) {
            super(itemView);
            imgVideo = itemView.findViewById(R.id.img_video);
            imgAvatar = itemView.findViewById(R.id.img_avatar);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvChannel = itemView.findViewById(R.id.tv_chanel);
            tvTime = itemView.findViewById(R.id.tv_time);
            tvVisits = itemView.findViewById(R.id.tv_visits);
            btnMore = itemView.findViewById(R.id.btn_more);


        }
    }






}
