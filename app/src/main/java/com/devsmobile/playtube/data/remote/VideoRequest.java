package com.devsmobile.playtube.data.remote;

import androidx.annotation.NonNull;

import com.devsmobile.playtube.data.model.Item;
import com.devsmobile.playtube.data.model.Keys;
import com.devsmobile.playtube.data.model.Main;
import com.devsmobile.playtube.inyection.api.IKeys;
import com.devsmobile.playtube.inyection.api.IVideo;
import com.devsmobile.playtube.inyection.callback.IDataKeys;
import com.devsmobile.playtube.inyection.callback.IDataVideo;
import com.devsmobile.playtube.util.Global;
import com.devsmobile.playtube.util.HelperData;
import com.devsmobile.playtube.util.Url;

import java.security.Key;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoRequest {
    private IDataVideo iDataVideo;
    private IDataKeys iDataKeys;
    // IDataVideoChannel iDataVideoChannel;



    public VideoRequest(IDataVideo IDataVideo) {
        this.iDataVideo = IDataVideo;
    }
    public VideoRequest(IDataKeys IDataKeys) {
        this.iDataKeys= IDataKeys;
    }


    //Obtiene los videos mas populares actualmente
    public void getMostPopularVideoData(String idCategory, String nextPage) {
        final IVideo videoServices = SingletonInstance.
                getRetrofitInstance().create(IVideo.class);

        String locale = Locale.getDefault().getCountry(); // Obtiene el lugar , Ejemplo  : GT
        Call<Main> videoCall = videoServices.getDataVideo(new Url().getUrlChartVideo(Global.CHART, HelperData.getApiKey(), Global.ALL_PART, Global.MAX_RESULTS, locale, idCategory, nextPage));
        videoCall.enqueue(new Callback<Main>() {
            @Override
            public void onResponse(@NonNull Call<Main> call, @NonNull Response<Main> response) {
                try {
                    if (response.body() != null && response.code() == 200) {
                        if (response.body().getItems().size() == 0)
                            iDataVideo.onGetDataVideoSuccess(response.body());
                        else
                            getChannelInfo(response.body());
                    } else
                        iDataVideo.onGetDataVideoFailure(null);

                } catch (Exception ex) {

                    iDataVideo.onGetDataVideoFailure(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Main> call, @NonNull Throwable t) {
                iDataVideo.onGetDataVideoFailure(t);

            }
        });
    }

    //Obtiene los avatares y banners de canales de  videos
    private void getChannelInfo(final Main main) {
        final IVideo videoServices = SingletonInstance.
                getRetrofitInstance().create(IVideo.class);

        String id = new HelperData().getID(main.getItems(), false).toString();


        Call<Main> videoCall = videoServices.getDataVideo(new Url().getUrlChannelsAvatarBanner(HelperData.getApiKey(), Global.CHANNEL_PART, id));

        videoCall.enqueue(new Callback<Main>() {
            @Override
            public void onResponse(@NonNull Call<Main> call, @NonNull Response<Main> response) {
                try {
                    if (response.body() != null && response.code() == 200) {
                        List<Item> dataResponse = new HelperData().getDataChannelURL(main.getItems(), response.body());
                        response.body().setNextPageToken(main.getNextPageToken());
                        response.body().setItems(dataResponse);
                        iDataVideo.onGetDataVideoSuccess(response.body());
                    } else
                        iDataVideo.onGetDataVideoFailure(null);

                } catch (Exception ex) {
                    iDataVideo.onGetDataVideoFailure(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Main> call, @NonNull Throwable t) {
                iDataVideo.onGetDataVideoFailure(t);
            }
        });
    }

    //Obtiene los videos de un canal ordenados por fecha
    public void getChannelVideoData(String channelID, String pageToken) {
        final IVideo videoServices = SingletonInstance.
                getRetrofitInstance().create(IVideo.class);


        Call<Main> call = videoServices.getDataVideo(new Url().getUrlIdVideoChannel(Global.DATE_PART, Global.ID_PART + "," + Global.SNIPPET_PART, HelperData.getApiKey(), channelID, Global.MAX_RESULTS, pageToken));

        call.enqueue(new Callback<Main>() {
            @Override
            public void onResponse(@NonNull Call<Main> call, @NonNull Response<Main> response) {
                try {
                    if (response.body() != null && response.code() == 200)
                        getDataVideoInfo(response.body(), true);
                    else
                        iDataVideo.onGetDataVideoFailure(null);

                } catch (Exception e) {
                    iDataVideo.onGetDataVideoFailure(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Main> call, @NonNull Throwable t) {
                iDataVideo.onGetDataVideoFailure(t);
            }
        });
    }

    //Obtiene informacion de varios videos
    public void getDataVideoInfo(Main main, boolean isObjectId) {
        final IVideo videoServices = SingletonInstance.
                getRetrofitInstance().create(IVideo.class);

        String id = new HelperData().getID(main.getItems(), isObjectId).toString();
        Call<Main> call = videoServices.getDataVideo(new Url().getUrlDataVideoChannel(Global.STATISTICS_PART + "," + Global.CONTENT_DETAILS_PART, HelperData.getApiKey(), id));

        call.enqueue(new Callback<Main>() {
            @Override
            public void onResponse(@NonNull Call<Main> call, @NonNull Response<Main> response) {
                try {
                    if (response.body() != null && response.code() == 200) {

                        List<Item> dataResponse = new HelperData().getDataWithInfoVideo(main.getItems(), response.body().getItems());
                        response.body().setItems(dataResponse);
                        response.body().setNextPageToken(main.getNextPageToken());
                        iDataVideo.onGetDataVideoSuccess(response.body());
                    } else {
                        iDataVideo.onGetDataVideoFailure(null);
                    }
                } catch (Exception ex) {
                    iDataVideo.onGetDataVideoFailure(null);
                }

            }

            @Override
            public void onFailure(@NonNull Call<Main> call, @NonNull Throwable t) {
                iDataVideo.onGetDataVideoFailure(t);
            }
        });
    }


    //Obtiene videos seguun la busqueda
    public void getQueryVideoData(String query, String idRelatedVideo, String pageToken, int maxResult) {
        final IVideo videoServices = SingletonInstance.
                getRetrofitInstance().create(IVideo.class);

        String type = "";
        if (idRelatedVideo.length() > 0)
            type = Global.TYPE_VIDEO;
        else
            type = Global.ALL_TYPES;


        int results = maxResult != 0 ? maxResult : Global.MAX_RESULTS;

        Call<Main> videoCall = videoServices.getDataVideo(new Url().getUrlQuery(query, HelperData.getApiKey(), Global.ID_PART + ',' +
                Global.SNIPPET_PART, type, results, idRelatedVideo, pageToken));


        videoCall.enqueue(new Callback<Main>() {
            @Override
            public void onResponse(@NonNull Call<Main> call, @NonNull Response<Main> response) {
                try {
                    if (response.body() != null && response.code() == 200)

                        getChannelInfo(response.body());

                    else
                        iDataVideo.onGetDataVideoFailure(null);

                } catch (Exception ex) {
                    iDataVideo.onGetDataVideoFailure(null);
                }

            }

            @Override
            public void onFailure(@NonNull Call<Main> call, @NonNull Throwable t) {
                iDataVideo.onGetDataVideoFailure(t);

            }
        });
    }
    //Obtiene informacion de una lista de reproduccion

    public void getPlayListItemData(String idPlayList, int maxResult) {
        final IVideo videoServices = SingletonInstance.
                getRetrofitInstance().create(IVideo.class);

        Call<Main> videoCall = videoServices.getDataVideo(new Url().getUrlPlayListItems(Global.ID_PART + "," + Global.SNIPPET_PART + "," + Global.STATUS_PART, HelperData.getApiKey(), idPlayList, maxResult));


        videoCall.enqueue(new Callback<Main>() {
            @Override
            public void onResponse(@NonNull Call<Main> call, @NonNull Response<Main> response) {
                try {
                    if (response.body() != null && response.code() == 200)
                        iDataVideo.onGetDataVideoSuccess(response.body());
                    else
                        iDataVideo.onGetDataVideoFailure(null);

                } catch (Exception ex) {
                    iDataVideo.onGetDataVideoFailure(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Main> call, @NonNull Throwable t) {
                iDataVideo.onGetDataVideoFailure(t);

            }
        });
    }

    //Obtiene todas las categorias de los vidos por region
    public void getCategories() {
        final IVideo videoServices = SingletonInstance.
                getRetrofitInstance().create(IVideo.class);


        String locale = Locale.getDefault().getCountry(); // Obtiene el lugar , Ejemplo  : GT

        Call<Main> videoCall = videoServices.getDataVideo(new Url().getUrlCategories(Global.ID_PART + "," + Global.SNIPPET_PART, locale, Locale.getDefault().getLanguage(), HelperData.getApiKey()));
        System.out.println("URL REQUEST : " +new Url().getUrlCategories(Global.ID_PART + "," + Global.SNIPPET_PART, locale, Locale.getDefault().getLanguage(), HelperData.getApiKey()));
        videoCall.enqueue(new Callback<Main>() {
            @Override
            public void onResponse(@NonNull Call<Main> call, @NonNull Response<Main> response) {

                try {
                    if (response.body() != null && response.code() == 200)
                        iDataVideo.onGetDataVideoSuccess(response.body());
                    else{

                        iDataVideo.onGetDataVideoFailure(null);
                    }
                } catch (Exception ex) {

                    iDataVideo.onGetDataVideoFailure(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Main> call, @NonNull Throwable t) {

                iDataVideo.onGetDataVideoFailure(t);

            }
        });
    }

    //Obtiene datos de un canal
    public void getChannelData(String idVideo) {
        final IVideo videoServices = SingletonInstance.
                getRetrofitInstance().create(IVideo.class);


        Call<Main> videoCall = videoServices.getDataVideo(new Url().getUrlChannelData(HelperData.getApiKey(), Global.ID_PART + "," + Global.SNIPPET_PART + "," + Global.STATISTICS_PART, idVideo));

        videoCall.enqueue(new Callback<Main>() {
            @Override
            public void onResponse(@NonNull Call<Main> call, @NonNull Response<Main> response) {
                try {
                    if (response.body() != null && response.code() == 200)
                        iDataVideo.onGetDataVideoSuccess(response.body());
                    else
                        iDataVideo.onGetDataVideoFailure(null);
                } catch (Exception ex) {
                    iDataVideo.onGetDataVideoFailure(null);
                }


            }

            @Override
            public void onFailure(@NonNull Call<Main> call, @NonNull Throwable t) {
                iDataVideo.onGetDataVideoFailure(t);
            }
        });
    }

    public void getVideoData(String idVideo) {
        final IVideo videoServices = SingletonInstance.
                getRetrofitInstance().create(IVideo.class);

        Call<Main> videoCall = videoServices.getDataVideo(new Url().getUrlSingleVideo(HelperData.getApiKey(), idVideo, Global.STATISTICS_PART));
        videoCall.enqueue(new Callback<Main>() {
            @Override
            public void onResponse(@NonNull Call<Main> call, @NonNull Response<Main> response) {
                try {
                    if (response.body() != null && response.code() == 200)
                        iDataVideo.onGetDataVideoSuccess(response.body());
                    else
                        iDataVideo.onGetDataVideoFailure(null);
                } catch (Exception ex) {
                    iDataVideo.onGetDataVideoFailure(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Main> call, @NonNull Throwable t) {
                iDataVideo.onGetDataVideoFailure(t);

            }
        });

    }
    public void getKeys() {
        final IKeys keys = SingletonInstance.
                getRetrofitInstance().create(IKeys.class);

        Call<Keys> videoCall = keys.getKeys(Global.URL_API + "keys");
        videoCall.enqueue(new Callback<Keys>() {
            @Override
            public void onResponse(@NonNull Call<Keys> call, @NonNull Response<Keys> response) {
                try {
                    if (response.body() != null && response.code() == 200)
                        iDataKeys.onGetDataKeysSuccess(response.body());
                    else
                        iDataVideo.onGetDataVideoFailure(null);
                } catch (Exception ex) {
                    iDataVideo.onGetDataVideoFailure(null);
                }
            }

            @Override
            public void onFailure(@NonNull Call<Keys> call, @NonNull Throwable t) {
                iDataVideo.onGetDataVideoFailure(t);

            }
        });

    }

}
