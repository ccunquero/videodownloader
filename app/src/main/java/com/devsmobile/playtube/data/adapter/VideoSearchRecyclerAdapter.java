package com.devsmobile.playtube.data.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.devsmobile.playtube.util.SnackBar;
import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.gson.internal.LinkedTreeMap;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.devsmobile.playtube.R;
import com.devsmobile.playtube.data.model.Item;
import com.devsmobile.playtube.inyection.callback.IOpenDialogChannel;
import com.devsmobile.playtube.inyection.callback.IOpenPlayList;
import com.devsmobile.playtube.inyection.callback.IPlayVideo;
import com.devsmobile.playtube.ui.main.Common;
import com.devsmobile.playtube.util.Global;
import com.devsmobile.playtube.util.Image;

public class VideoSearchRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Object> videoModelArrayList;

    private IPlayVideo iPlayVideo;
    private IOpenDialogChannel iOpenDialogChannel;
    private IOpenPlayList iOpenPlayList;
    private Context context ;

    public VideoSearchRecyclerAdapter(Context context, List<Object> videoModelArrayList, IPlayVideo iPlayVideo,
                                      IOpenDialogChannel iOpenDialogChannel, IOpenPlayList iOpenPlayList) {

        this.videoModelArrayList = getArrayModel( videoModelArrayList);
        this.iPlayVideo = iPlayVideo;
        this.iOpenDialogChannel = iOpenDialogChannel;
        this.iOpenPlayList = iOpenPlayList;
        this.context = context;

    }
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        if (viewType == Global.UNIFIED_NATIVE_AD_VIEW_TYPE) {
            LayoutInflater layoutInflaterUnified = LayoutInflater.from(parent.getContext());
            View viewUnified = layoutInflaterUnified.inflate(R.layout.ad_unifed_small, parent, false);
            return new UnifiedNativeAdViewHolder(viewUnified);
        }else{
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_video_search_list, parent, false);
            return new YoutubeViewHolder(view);
        }



    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {

        int viewType = getItemViewType(position);

        if(viewType == Global.UNIFIED_NATIVE_AD_VIEW_TYPE){

            if( videoModelArrayList.get(position) != null){
                UnifiedNativeAd nativeAd = (UnifiedNativeAd) videoModelArrayList.get(position);
                new Common(context).populateNativeAdViewSmall(nativeAd, ((UnifiedNativeAdViewHolder) viewHolder).getAdView());
            }else{
                new Common(context).placeHolderAdsViewSmall(((UnifiedNativeAdViewHolder) viewHolder).getAdView());
            }

        }else if(viewType == Global.ITEM_VIEW_TYPE){
            final Item itemModel = (Item) videoModelArrayList.get(position);
            YoutubeViewHolder holder = (YoutubeViewHolder) viewHolder;
            holder.tvTitle.setText(itemModel.getSnippet().getTitle());
            holder.tvChannel.setText(itemModel.getSnippet().getChannelTitle());

            LinkedTreeMap<Object, Object> id = (LinkedTreeMap) itemModel.getId();
            if (Objects.equals(id.get("kind"), Global.KIND_CHANNEL)) {
                holder.imgChannel.setVisibility(View.VISIBLE);
                holder.imgVideo.setVisibility(View.GONE);
                holder.containerImagePlayList.setVisibility(View.GONE);
                Image.loadImage(holder.imgChannel, itemModel.getSnippet().getThumbnails().getMedium().getUrl());


            } else if (Objects.equals(id.get("kind"), Global.KIND_VIDEO)) {
                holder.imgVideo.setVisibility(View.VISIBLE);
                holder.imgChannel.setVisibility(View.GONE);
                holder.containerImagePlayList.setVisibility(View.GONE);
                Image.loadImage(holder.imgVideo, itemModel.getSnippet().getThumbnails().getMedium().getUrl());

            } else if (Objects.equals(id.get("kind"), Global.KIND_PLAYLIST)) {
                holder.imgVideo.setVisibility(View.GONE);
                holder.imgChannel.setVisibility(View.GONE);
                holder.containerImagePlayList.setVisibility(View.VISIBLE);
                Image.loadImage(holder.imgPlayList, itemModel.getSnippet().getThumbnails().getMedium().getUrl());

            }

            holder.containerItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    if (id.get("videoId") != null) {
                        if(itemModel.getStatistics() == null){
                            SnackBar.error(context,context.getString(R.string.error_general));
                            return;
                        }

                        bundle.putString("VIDEO_ID", Objects.requireNonNull(id.get("videoId")).toString());
                        bundle.putString("VIDEO_TITLE", itemModel.getSnippet().getTitle());
                        bundle.putLong("VIDEO_LIKE", itemModel.getStatistics().getLikeCount() != null ?
                                Long.parseLong(itemModel.getStatistics().getLikeCount()) : 0);
                        bundle.putLong("VIDEO_DISLIKE", itemModel.getStatistics().getDislikeCount() != null ?
                                Long.parseLong(itemModel.getStatistics().getDislikeCount()) : 0);
                        bundle.putLong("VIDEO_VIEW", itemModel.getStatistics().getViewCount() != null ?
                                Long.parseLong(itemModel.getStatistics().getViewCount()) : 0);
                        bundle.putString("VIDEO_DESCRIPTION", itemModel.getSnippet().getDescription());
                        bundle.putString("CHANNEL_ID", itemModel.getSnippet().getChannelId());
                        bundle.putString("VIDEO_URL_THUMBNAIL", itemModel.getSnippet().getThumbnails().getMedium().getUrl());
                        iPlayVideo.onPlayVideo(bundle);
                    } else {
                        SnackBar.error(context,context.getString(R.string.error_general));

                    }
                }
            });

            holder.imgChannel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Bundle bundle = new Bundle();
                    if(itemModel.getStatistics() == null){
                        SnackBar.error(context,context.getString(R.string.error_general));
                        return;
                    }
                    bundle.putString("CHANNEL_URL", itemModel.getSnippet().getUrlBanner());
                    bundle.putString("CHANNEL_TITLE", itemModel.getSnippet().getChannelTitle());
                    bundle.putString("CHANNEL_AVATAR", itemModel.getSnippet().getUrlAvatar());
                    bundle.putString("CHANNEL_PROFILE_COLOR", itemModel.getSnippet().getChannelProfileColor());
                    bundle.putLong("CHANNEL_VIEWS_COUNT", itemModel.getStatistics().getViewCountChannel() != null ?
                            Long.parseLong(itemModel.getStatistics().getViewCountChannel()) : 0);
                    bundle.putLong("CHANNEL_SUBSCRIBER_COUNT", itemModel.getStatistics().getSubscriberCount() != null ?
                            Long.parseLong(itemModel.getStatistics().getSubscriberCount()) : 0);
                    bundle.putLong("CHANNEL_VIDEO_COUNT", itemModel.getStatistics().getVideoCount() != null ?
                            Long.parseLong(itemModel.getStatistics().getVideoCount()) : 0);
                    bundle.putString("CHANNEL_ID", itemModel.getSnippet().getChannelId());
                    iOpenDialogChannel.onOpenDialogChannel(bundle);

                }
            });
            holder.containerImagePlayList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString("PLAYLIST_ID", Objects.requireNonNull(id.get("playlistId")).toString());
                    iOpenPlayList.onShowPlayList(bundle);
                }
            });


        }
    }
    public void updateRecycler(List<Object> data){
        this.videoModelArrayList.clear();
        this.videoModelArrayList.addAll(data);
        this.notifyDataSetChanged();

    }
    private List<Object> getArrayModel(List<Object> list) {
        List<Object> itemList = new ArrayList<>();
        for (Object item : list) {
            if(item instanceof  Item ){
                Item currentItem = (Item) item;
                if(!currentItem.getSnippet().getLiveBroadcastContent().equals(Global.BROADCAST_CONTENT_LIVE)){
                    itemList.add(currentItem);
                }
            }else
                itemList.add(item);


        }
        return itemList;

    }



    @Override
    public int getItemCount() {
        return videoModelArrayList != null ? videoModelArrayList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {

        Object recyclerViewItem = videoModelArrayList.get(position);
        if (recyclerViewItem instanceof UnifiedNativeAd || recyclerViewItem == null) {
            return Global.UNIFIED_NATIVE_AD_VIEW_TYPE;
        }else{
            return  Global.ITEM_VIEW_TYPE;
        }
    }
    private class YoutubeViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle, tvChannel;

        private ImageView imgVideo;

        private LinearLayout containerItem;
        private RoundedImageView imgChannel;
        private RelativeLayout containerImagePlayList;
        private ImageView imgPlayList;


        private YoutubeViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvChannel = itemView.findViewById(R.id.tv_channel_video);
            imgVideo = itemView.findViewById(R.id.img_video);
            containerItem = itemView.findViewById(R.id.container_item);
            imgChannel = itemView.findViewById(R.id.img_channel);
            containerImagePlayList = itemView.findViewById(R.id.containerImagePlayList);
            imgPlayList = itemView.findViewById(R.id.img_playlist);
        }
    }


}
