package com.devsmobile.playtube.data.model;


public class Download {
    private int idDownload;
    private String idVideo;
    private String titleVideo;
    private String dateDownload;
    private String sizeDownload;
    private String stateDownload;
    private String url;
    private String path;
    private String urlThumbnail;
    private String progress;


    public Download() {
    }

    public int getIdDownload() {
        return idDownload;
    }

    public void setIdDownload(int idDownload) {
        this.idDownload = idDownload;
    }

    public String getIdVideo() {
        return idVideo;
    }

    public void setIdVideo(String idVideo) {
        this.idVideo = idVideo;
    }

    public String getTitleVideo() {
        return titleVideo;
    }

    public void setTitleVideo(String titleVideo) {
        this.titleVideo = titleVideo;
    }

    public String getDateDownload() {
        return dateDownload;
    }

    public void setDateDownload(String dateDownload) {
        this.dateDownload = dateDownload;
    }

    public String getSizeDownload() {
        return sizeDownload;
    }

    public void setSizeDownload(String sizeDownload) {
        this.sizeDownload = sizeDownload;
    }

    public String getStateDownload() {
        return stateDownload;
    }

    public void setStateDownload(String stateDownload) {
        this.stateDownload = stateDownload;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getUrlThumbnail() {
        return urlThumbnail;
    }

    public void setUrlThumbnail(String urlThumbnail) {
        this.urlThumbnail = urlThumbnail;
    }


    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }
}

