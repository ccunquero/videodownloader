package com.devsmobile.playtube.data.services;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.IBinder;

import androidx.annotation.Nullable;

public class UpdateService extends Service {

    @Override
    public void onCreate() {
        super.onCreate();
        // REGISTER RECEIVER THAT HANDLES SCREEN ON AND SCREEN OFF LOGIC
        IntentFilter filter = new IntentFilter(Intent.ACTION_SCREEN_ON);
        filter.addAction(Intent.ACTION_SCREEN_OFF);
        BroadcastReceiver mReceiver = new ScreenReceiver();
        registerReceiver(mReceiver, filter);
    }


    @Override
    public void onStart(Intent intent, int startId) {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        boolean screenOn = intent.getBooleanExtra("screen_state", false);
        if (!screenOn) {
            // YOUR CODE

            System.out.println("SCREEN OFF IN THE SERVICE");
        } else {
            // YOUR CODE
            System.out.println("SCREENN ON IN THE SERVICE ");
        }


        return null;
    }
}