package com.devsmobile.playtube.data.model;

public class DetailPlayList {
    private int idDetailPlayList;
    private int idPlayList;
    private String idVideo;
    private String tittleVideo;
    private String channelName;
    private String urlThumbnail;
    private String idChannel;

    public int getIdDetailPlayList() {
        return idDetailPlayList;
    }

    public void setIdDetailPlayList(int idDetailPlayList) {
        this.idDetailPlayList = idDetailPlayList;
    }

    public int getIdPlayList() {
        return idPlayList;
    }

    public void setIdPlayList(int idPlayList) {
        this.idPlayList = idPlayList;
    }

    public String getIdVideo() {
        return idVideo;
    }

    public void setIdVideo(String idVideo) {
        this.idVideo = idVideo;
    }

    public String getTittleVideo() {
        return tittleVideo;
    }

    public void setTittleVideo(String tittleVideo) {
        this.tittleVideo = tittleVideo;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getUrlThumbnail() {
        return urlThumbnail;
    }

    public void setUrlThumbnail(String urlThumbnail) {
        this.urlThumbnail = urlThumbnail;
    }

    public String getIdChannel() {
        return idChannel;
    }

    public void setIdChannel(String idChannel) {
        this.idChannel = idChannel;
    }
}
