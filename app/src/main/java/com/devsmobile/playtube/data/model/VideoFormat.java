package com.devsmobile.playtube.data.model;

import androidx.annotation.NonNull;

public class VideoFormat {
    private String formatId;
    private int height;
    private int width;
    private String format;
    private String extension;
    private String formatNote;
    private int abr; //  tasa de bits de audio promedio en KBit / s


    public VideoFormat(String formatId, int height, int width, String format, String extension, String formatNote, int abr) {
        this.formatId = formatId;
        this.height = height;
        this.width = width;
        this.format = format;
        this.extension = extension;
        this.formatNote = formatNote;
        this.abr = abr;
    }

    public String getFormatId() {
        return formatId;
    }

    public void setFormatId(String formatId) {
        this.formatId = formatId;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getFormatNote() {
        return formatNote;
    }

    public void setFormatNote(String formatNote) {
        this.formatNote = formatNote;
    }

    public int getAbr() {
        return abr;
    }

    public void setAbr(int abr) {
        this.abr = abr;
    }

    @NonNull
    @Override
    public String toString() {
        return this.formatNote + this.extension;
    }
}
