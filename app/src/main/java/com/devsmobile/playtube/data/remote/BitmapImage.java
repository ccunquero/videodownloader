package com.devsmobile.playtube.data.remote;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import java.net.URL;

import com.devsmobile.playtube.inyection.callback.IGetImage;

public class BitmapImage extends AsyncTask<Object, Void, Bitmap> {


    public Bitmap doInBackground(Object... objects) {
        try {
            URL url = new URL((String) objects[0]);
            IGetImage iGetImage = (IGetImage) objects[1];
            Bitmap originalBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
            //    Bitmap croppedBitmap = Bitmap.createBitmap(originalBitmap, 0, 10, originalBitmap.getWidth()  , originalBitmap.getHeight()-10);
            //   Bitmap resized = Bitmap.createScaledBitmap(croppedBitmap, croppedBitmap.getWidth() , croppedBitmap.getHeight() + 40, true);
            iGetImage.onGetImage(originalBitmap);
            return null;
        } catch (Exception e) {
            IGetImage iGetImage = (IGetImage) objects[1];
            iGetImage.onErrorImage(null);
            return null;
        }
    }

    protected void onPostExecute(IGetImage bitmap) {
        // TODO: check this.exception
        // TODO: do something with the feed
    }
}