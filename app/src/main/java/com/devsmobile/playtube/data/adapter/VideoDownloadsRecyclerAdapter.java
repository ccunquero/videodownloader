package com.devsmobile.playtube.data.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.github.abdularis.buttonprogress.DownloadButtonProgress;

import java.util.List;

import com.devsmobile.playtube.data.model.Download;
import com.devsmobile.playtube.inyection.callback.IViewHolderItemDownload;
import com.devsmobile.playtube.R;
import com.devsmobile.playtube.util.Global;
import com.devsmobile.playtube.util.Image;

public class VideoDownloadsRecyclerAdapter extends RecyclerView.Adapter<VideoDownloadsRecyclerAdapter.YoutubeViewHolder> {

    private List<Download> videoModelArrayList;
    private IViewHolderItemDownload iViewHolderItemDownload;

    public VideoDownloadsRecyclerAdapter(Context context, List<Download> videoModelArrayList, IViewHolderItemDownload iViewHolderItemDownload) {

        this.videoModelArrayList = videoModelArrayList;
        this.iViewHolderItemDownload = iViewHolderItemDownload;
    }

    @NonNull
    @Override
    public YoutubeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_download_list, parent, false);
        return new YoutubeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final YoutubeViewHolder holder, final int position) {

        final Download itemModel = videoModelArrayList.get(position);

        holder.tvTitle.setText(itemModel.getTitleVideo());
        Image.loadImage(holder.imgVideo, itemModel.getUrlThumbnail());
        holder.buttonProgress.setCurrentProgress(0);
        holder.tvProgress.setVisibility(View.GONE);

        switch (itemModel.getStateDownload()) {
            case Global.DOWNLOAD_IN_HOLD:
                holder.buttonProgress.setIdle();
                holder.tvState.setText("Iniciando...");
                break;
            case Global.DOWNLOAD_IN_PROGRESS:
                holder.buttonProgress.setDeterminate();
                holder.tvState.setText("Descargando");
                holder.tvProgress.setVisibility(View.VISIBLE);
                break;
            case Global.DOWNLOAD_FINISHED:
                holder.buttonProgress.setFinish();
                holder.tvState.setText("Finalizado");
                break;
            case Global.DOWNLOAD_FAILURE:
                holder.buttonProgress.setIdle();
                holder.tvState.setText("Error al descargar");
                break;
        }

        holder.tvProgress.setText(itemModel.getProgress() + "%");
        iViewHolderItemDownload.onCreateView(itemModel.getUrl(), holder);


    }


    @Override
    public int getItemCount() {
        return videoModelArrayList != null ? videoModelArrayList.size() : 0;
    }

    public class YoutubeViewHolder extends RecyclerView.ViewHolder {

        public TextView tvTitle, tvProgress, tvState;
        public ImageView imgVideo;
        public DownloadButtonProgress buttonProgress;


        private YoutubeViewHolder(View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.tv_title);
            imgVideo = itemView.findViewById(R.id.img_video);
            buttonProgress = itemView.findViewById(R.id.download_progress);
            tvProgress = itemView.findViewById(R.id.tv_progress);
            tvState = itemView.findViewById(R.id.tv_state);


        }
    }


}
