package com.devsmobile.playtube.data.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.Objects;

import com.devsmobile.playtube.util.Global;

public class ScreenReceiver extends BroadcastReceiver {

    public static boolean screenOff;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Objects.equals(intent.getAction(), Intent.ACTION_SCREEN_OFF)) {
            screenOff = true;
            System.out.println("SCREEN OFF IN THE BROADCAST");
            if(Global.PUBLISH_TO_PLAY_STORE)
                Global.YOUTUBE_PLAYER.pause();
        } else if (Objects.requireNonNull(intent.getAction()).equals(Intent.ACTION_SCREEN_ON)) {
            screenOff = false;
            System.out.println("SCRENN ON IN THE BORADCAST");
            if(Global.PUBLISH_TO_PLAY_STORE)
                Global.YOUTUBE_PLAYER.play();
        }
        Intent i = new Intent(context, UpdateService.class);
         i.putExtra("screen_state", screenOff);
         context.startService(i);
    }


}

