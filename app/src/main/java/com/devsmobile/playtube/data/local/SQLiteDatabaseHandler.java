package com.devsmobile.playtube.data.local;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.devsmobile.playtube.data.model.DetailPlayList;
import com.devsmobile.playtube.data.model.Download;
import com.devsmobile.playtube.data.model.PlayList;
import com.devsmobile.playtube.data.model.Video;
import com.devsmobile.playtube.util.Global;

public class SQLiteDatabaseHandler extends SQLiteOpenHelper {


    public SQLiteDatabaseHandler(Context context) {
        super(context, Global.DATABASE_NAME, null, Global.DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String tblDownload = "CREATE TABLE " + Global.TABLE_DOWNLOAD + " ( " +
                "idDownload INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "idVideo TEXT," +
                "titleVideo TEXT, " +
                "dateDownload TEXT," +
                "sizeDownload TEXT," +
                "stateDownload TEXT," +
                "url TEXT ," +
                "progress TEXT," +
                "path TEXT," +
                "urlThumbnail TEXT)";


        String tblPlayList = "CREATE TABLE " + Global.TABLE_PLAY_LIST + " (" +
                "idPlayList INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "title TEXT NOT NULL," +
                "urlThumbnail TEXT" +
                ");";


        String tblDetailPlayList = "CREATE TABLE " + Global.TABLE_DETAIL_PLAY_LIST + " (" +
                "idDetailPlayList INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "idPlayList INTEGER NOT NULL," +
                "idVideo TEXT NOT NULL, " +
                "titleVideo TEXT NOT NULL," +
                "channelName TEXT NOT NULL," +
                "urlThumbnail TEXT ," +
                "idChannel TEXT NOT NULL, " +
                " FOREIGN KEY(idPlayList) REFERENCES " + Global.TABLE_PLAY_LIST + "(idPlayList)" +
                ");";

        String tblVideoSaved = "CREATE TABLE " + Global.TABLE_VIDEO + " ( " +
                "idVideo INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                "idVideoYouTube TEXT NOT NULL , " +
                "titleVideo TEXT NOT NULL," +
                "channelName TEXT NOT NULL," +
                "urlThumbnail TEXT NOT NULL, " +
                "idChannel TEXT NOT NULL," +
                "type TEXT NOT NULL," +
                "date DATE DEFAULT (datetime('now','localtime'))) ";

        sqLiteDatabase.execSQL(tblDownload);
        sqLiteDatabase.execSQL(tblPlayList);
        sqLiteDatabase.execSQL(tblDetailPlayList);
        sqLiteDatabase.execSQL(tblVideoSaved);


    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Global.TABLE_DOWNLOAD);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Global.TABLE_DETAIL_PLAY_LIST);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Global.TABLE_PLAY_LIST);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Global.TABLE_VIDEO);


        this.onCreate(sqLiteDatabase);
    }


    public void deleteAllRows(String table) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + table);
        db.close();
    }


    public List<DetailPlayList> getDetailPlayList(int idPlayList) {

        List<DetailPlayList> list = new LinkedList<>();
        String query = "SELECT  idDetailPlayList, idPlayList, idVideo ,  titleVideo, channelName, urlThumbnail, idChannel FROM " +
                Global.TABLE_DETAIL_PLAY_LIST + " WHERE idPlayList = ?  ORDER BY idDetailPlayList desc";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(idPlayList)});

        if (cursor.moveToFirst()) {
            do {
                DetailPlayList detailPlayList = new DetailPlayList();
                detailPlayList.setIdDetailPlayList(cursor.getInt(0));
                detailPlayList.setIdPlayList(cursor.getInt(1));
                detailPlayList.setIdVideo(cursor.getString(2));
                detailPlayList.setTittleVideo(cursor.getString(3));
                detailPlayList.setChannelName(cursor.getString(4));
                detailPlayList.setUrlThumbnail(cursor.getString(5));
                detailPlayList.setIdChannel(cursor.getString(6));

                list.add(detailPlayList);
            } while (cursor.moveToNext());
        }
        db.close();
        cursor.close();
        return list;

    }

    public List<PlayList> getPlayLists() {

        List<PlayList> list = new ArrayList<>();
        String query = "SELECT  playList.idPlayList, playList.title, playList.urlThumbnail ,COUNT(idDetailPlayList) AS videoCount FROM "
                + Global.TABLE_PLAY_LIST + " playList LEFT JOIN " + Global.TABLE_DETAIL_PLAY_LIST
                + " detailPlayList ON playList.idPlayList = detailPlayList.idPlayList GROUP BY playList.idPlayList  ORDER BY playList.idPlayList desc";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);


        if (cursor.moveToFirst()) {
            do {
                PlayList playList = new PlayList();
                playList.setIdPlayList(cursor.getInt(0));
                playList.setTitle(cursor.getString(1));
                playList.setUrlThumbnail(cursor.getString(2));
                playList.setVideoCount(cursor.getInt(3));

                list.add(playList);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return list;
    }

    public long insertPlayList(PlayList playList) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("title", playList.getTitle());
        values.put("urlThumbnail", playList.getUrlThumbnail());

        long result = db.insert(Global.TABLE_PLAY_LIST, null, values);
        db.close();
        return result;
    }


    public long insertDetailPlayList(DetailPlayList detailPlayList) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("idPlayList", detailPlayList.getIdPlayList());
        values.put("titleVideo", detailPlayList.getTittleVideo());
        values.put("channelName", detailPlayList.getChannelName());
        values.put("urlThumbnail", detailPlayList.getUrlThumbnail());
        values.put("idChannel", detailPlayList.getIdChannel());
        values.put("idVideo", detailPlayList.getIdVideo());
        // insert
        long result = db.insert(Global.TABLE_DETAIL_PLAY_LIST, null, values);

        db.execSQL("UPDATE " + Global.TABLE_PLAY_LIST + " SET urlThumbnail='" +
                detailPlayList.getUrlThumbnail() + "' WHERE idPlayList=" + detailPlayList.getIdPlayList());

        db.close();

        return result;
    }

    public boolean deletePlayList(int idPlayList) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(Global.TABLE_PLAY_LIST, "idPlayList" + "=?", new String[]{String.valueOf(idPlayList)}) > 0;
    }

    public boolean deleteDetailPlayList(int idDetailPlayList) {
        SQLiteDatabase db = getWritableDatabase();
        return db.delete(Global.TABLE_DETAIL_PLAY_LIST, "idDetailPlayList" + "=?", new String[]{String.valueOf(idDetailPlayList)}) > 0;
    }


    public long insertVideo(Video video) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("idVideoYouTube", video.getIdVideoYouTube());
        values.put("titleVideo", video.getTitleVideo());
        values.put("channelName", video.getChannelName());
        values.put("urlThumbnail", video.getUrlThumbnail());
        values.put("idChannel", video.getIdChannel());
        values.put("type", video.getType());
         long result = db.insert(Global.TABLE_VIDEO, null, values);
         db.close();
         return  result;


    }


    public List<Video> getVideos(String type) {
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT  idVideoYouTube, titleVideo, channelName, urlThumbnail, idChannel FROM " +
                Global.TABLE_VIDEO + " WHERE TYPE = ? ORDER BY idVideo desc";
        Cursor cursor = db.rawQuery(query, new String[]{type});

        List<Video> list = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Video video = new Video();
                video.setIdVideoYouTube(cursor.getString(0));
                video.setTitleVideo(cursor.getString(1));
                video.setChannelName(cursor.getString(2));
                video.setUrlThumbnail(cursor.getString(3));
                video.setIdChannel(cursor.getString(4));

                list.add(video);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
        return list;
    }


    // ----------------YA NO SON UTILIZADAS---------
    public List<Download> getDownloads() {

        List<Download> list = new LinkedList<>();
        String query = "SELECT  idDownload, idVideo, titleVideo, dateDownload, sizeDownload, stateDownload, url, path, urlThumbnail, progress FROM " +
                Global.TABLE_DOWNLOAD + " ORDER BY idDownload desc";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);


        if (cursor.moveToFirst()) {
            do {
                Download download = new Download();
                download.setIdDownload(Integer.parseInt(cursor.getString(0)));
                download.setIdVideo(cursor.getString(1));
                download.setTitleVideo(cursor.getString(2));
                download.setDateDownload(cursor.getString(3));
                download.setSizeDownload(cursor.getString(4));
                download.setStateDownload(cursor.getString(5));
                download.setUrl(cursor.getString(6));
                download.setPath(cursor.getString(7));
                download.setUrlThumbnail(cursor.getString(8));
                download.setProgress(cursor.getString(9));

                list.add(download);
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return list;
    }

    public long insert(Download download) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("idVideo", download.getIdVideo());
        values.put("titleVideo", download.getTitleVideo());
        values.put("dateDownload", download.getDateDownload());
        values.put("sizeDownload", download.getSizeDownload());
        values.put("stateDownload", download.getStateDownload());
        values.put("url", download.getUrl());
        values.put("path", download.getPath());
        values.put("progress", "0");
        values.put("urlThumbnail", download.getUrlThumbnail());

        // insert
        long result = db.insert(Global.TABLE_DOWNLOAD, null, values);
        db.close();
        return result;
    }

    public void updatePlayer(Download download) {

        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("idDownload", download.getIdDownload());
        values.put("idVideo", download.getIdVideo());
        values.put("titleVideo", download.getTitleVideo());
        values.put("dateDownload", download.getDateDownload());
        values.put("sizeDownload", download.getSizeDownload());
        values.put("stateDownload", download.getStateDownload());
        values.put("url", download.getUrl());
        values.put("path", download.getPath());
        values.put("urlThumbnail", download.getUrlThumbnail());

        db.update(Global.TABLE_DOWNLOAD, // table
                values, // column/value
                "idDownload = ?", // selections
                new String[]{String.valueOf(download.getIdDownload())});

        db.close();


    }

    public void updateState(String state, float progress, long id) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("stateDownload", state);
        values.put("progress", String.valueOf(progress));
        db.update(Global.TABLE_DOWNLOAD, // table
                values, // column/value
                "idDownload = ?", // selections
                new String[]{String.valueOf(id)});

        db.close();
    }

    public void deleteAllDownload() {

        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DELETE FROM " + Global.TABLE_DOWNLOAD);
        db.close();

    }

    public void delete(Download download) {
        // Get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Global.TABLE_DOWNLOAD, "idDownload = ?", new String[]{String.valueOf(download.getIdDownload())});
        db.close();
    }

    public Download getDownloadById(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String[] columns = {"idDownload", "idVideo", "titleVideo", "dateDownload", "sizeDownload", "stateDownload, url, path, urlThumbnail"};
        Cursor cursor = db.query(Global.TABLE_DOWNLOAD, // a. table
                columns, // b. column names
                " idDownload = ?", // c. selections
                new String[]{String.valueOf(id)}, // d. selections args
                null, // e. group by
                null, // f. having
                null, // g. order by
                null); // h. limit

        if (cursor != null)
            cursor.moveToFirst();

        Download download = new Download();
        assert cursor != null;
        download.setIdDownload(Integer.parseInt(cursor.getString(0)));
        download.setIdVideo(cursor.getString(1));
        download.setTitleVideo(cursor.getString(2));
        download.setDateDownload(cursor.getString(3));
        download.setSizeDownload(cursor.getString(4));
        download.setStateDownload(cursor.getString(5));
        download.setUrl(cursor.getString(6));
        download.setPath(cursor.getString(7));
        download.setUrlThumbnail(cursor.getString(8));

        db.close();

        return download;
    }


}
