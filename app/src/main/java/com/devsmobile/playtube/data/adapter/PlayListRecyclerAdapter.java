package com.devsmobile.playtube.data.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.util.List;

import com.devsmobile.playtube.data.model.DetailPlayList;
import com.devsmobile.playtube.data.model.Item;
import com.devsmobile.playtube.data.model.PlayList;
import com.devsmobile.playtube.data.model.Video;
import com.devsmobile.playtube.inyection.callback.IOpenPlayList;
import com.devsmobile.playtube.inyection.callback.IPlayVideo;
import com.devsmobile.playtube.R;
import com.devsmobile.playtube.ui.main.Common;
import com.devsmobile.playtube.util.Global;
import com.devsmobile.playtube.util.Image;

public class PlayListRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private List<Object> videoModelArrayList;
    private IPlayVideo iPlayVideo;

    private IOpenPlayList iOpenPlayList;


    public PlayListRecyclerAdapter(Context context, List<Object> videoModelArrayList, IPlayVideo iPlayVideo,
                                    IOpenPlayList iOpenPlayList) {
        this.context = context;

        this.videoModelArrayList = videoModelArrayList;
        this.iPlayVideo = iPlayVideo;
        this.iOpenPlayList = iOpenPlayList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        if (viewType == Global.UNIFIED_NATIVE_AD_VIEW_TYPE) {
            LayoutInflater layoutInflaterUnified = LayoutInflater.from(parent.getContext());
            View viewUnified = layoutInflaterUnified.inflate(R.layout.ad_unifed_small, parent, false);
            return new UnifiedNativeAdViewHolder(viewUnified);
        }else{
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_play_list, parent, false);
            return new YoutubeViewHolder(view);
        }

    }


    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int position) {

        int viewType = getItemViewType(position);

        if (viewType == Global.ITEM_VIDEO_SEARCH_TYPE) {
            final Item itemModel = (Item) videoModelArrayList.get(position);
            YoutubeViewHolder holder = (YoutubeViewHolder) viewHolder;

            holder.tvTitle.setText(itemModel.getSnippet().getTitle());
            holder.tvDescription.setText(itemModel.getSnippet().getDescription());

            Image.loadImage(holder.imgVideo, itemModel.getSnippet().getThumbnails().getMedium().getUrl());

            holder.containerItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Bundle bundle = new Bundle();
                    bundle.putString("VIDEO_ID", itemModel.getSnippet().getResourceId().getVideoId());
                    bundle.putString("VIDEO_TITLE", itemModel.getSnippet().getTitle());
                    bundle.putLong("VIDEO_LIKE", itemModel.getStatistics().getLikeCount() != null ?
                            Long.parseLong(itemModel.getStatistics().getLikeCount()) : 0);
                    bundle.putLong("VIDEO_DISLIKE", itemModel.getStatistics().getDislikeCount() != null ?
                            Long.parseLong(itemModel.getStatistics().getDislikeCount()) : 0);
                    bundle.putLong("VIDEO_VIEW", itemModel.getStatistics().getViewCount() != null ?
                            Long.parseLong(itemModel.getStatistics().getViewCount()) : 0);
                    bundle.putString("VIDEO_DESCRIPTION", itemModel.getSnippet().getDescription());
                    bundle.putString("CHANNEL_ID", itemModel.getSnippet().getChannelId());
                    bundle.putString("VIDEO_URL_THUMBNAIL", itemModel.getSnippet().getThumbnails().getMedium().getUrl());
                    iPlayVideo.onPlayVideo(bundle);

                }
            });
        } else if (viewType == Global.ITEM_PLAYLIST_TYPE) {

            final PlayList itemModel = (PlayList) videoModelArrayList.get(position);
            YoutubeViewHolder holder = (YoutubeViewHolder) viewHolder;


            holder.tvTitle.setText(itemModel.getTitle());
            holder.tvDescription.setText(this.context.getResources().getString(R.string.videos_with_points) +
                    " "+
                    itemModel.getVideoCount());

            String url = itemModel.getUrlThumbnail() != null ? itemModel.getUrlThumbnail() : "null";


            Image.loadImage(holder.imgVideo, url);
            holder.containerItem.setOnClickListener(view -> {
                Bundle bundle = new Bundle();

                bundle.putInt("ID_PLAY_LIST", itemModel.getIdPlayList());
                bundle.putString("TITLE_PLAY_LIST", itemModel.getTitle());
                iOpenPlayList.onShowPlayList(bundle);

            });
        } else if (viewType == Global.ITEM_DETAIL_PLAYLIST_TYPE) {
            final DetailPlayList itemModel = (DetailPlayList) videoModelArrayList.get(position);
            YoutubeViewHolder holder = (YoutubeViewHolder) viewHolder;


            holder.tvTitle.setText(itemModel.getTittleVideo());
            holder.tvDescription.setText(itemModel.getChannelName());
            String url = itemModel.getUrlThumbnail() != null ? itemModel.getUrlThumbnail() : "";
            Image.loadImage(holder.imgVideo, url);

            holder.containerItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString("CHANNEL_ID", itemModel.getIdChannel());
                    bundle.putString("VIDEO_ID", itemModel.getIdVideo());
                    bundle.putString("VIDEO_TITLE", itemModel.getTittleVideo());
                    bundle.putString("VIDEO_URL_THUMBNAIL", itemModel.getUrlThumbnail());
                    bundle.putString("CHANNEL_ID", itemModel.getIdChannel());
                    bundle.putBoolean("GET_DATA_VIDEO", true);
                    iPlayVideo.onPlayVideo(bundle);
                }
            });

        } else if (viewType == Global.ITEM_PLAYLIST_VIDEO_TYPE) {
            final Video itemModel = (Video) videoModelArrayList.get(position);
            YoutubeViewHolder holder = (YoutubeViewHolder) viewHolder;


            holder.tvTitle.setText(itemModel.getTitleVideo());
            holder.tvDescription.setText(itemModel.getChannelName());
            String url = itemModel.getUrlThumbnail() != null && itemModel.getUrlThumbnail().length() > 0 ? itemModel.getUrlThumbnail() : "null";
            Image.loadImage(holder.imgVideo, url);

            holder.containerItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString("CHANNEL_ID", itemModel.getIdChannel());
                    bundle.putString("VIDEO_ID", itemModel.getIdVideoYouTube());
                    bundle.putString("VIDEO_TITLE", itemModel.getTitleVideo());
                    bundle.putString("VIDEO_URL_THUMBNAIL", itemModel.getUrlThumbnail());
                    bundle.putBoolean("GET_DATA_VIDEO", true);
                    iPlayVideo.onPlayVideo(bundle);
                }
            });
        }else if (viewType == Global.UNIFIED_NATIVE_AD_VIEW_TYPE){
            if( videoModelArrayList.get(position) != null){
                UnifiedNativeAd nativeAd = (UnifiedNativeAd) videoModelArrayList.get(position);
                new Common(context).populateNativeAdViewSmall(nativeAd, ((UnifiedNativeAdViewHolder) viewHolder).getAdView());
            }else{
                new Common(context).placeHolderAdsViewSmall(((UnifiedNativeAdViewHolder) viewHolder).getAdView());
            }

        }
    }

    @Override
    public int getItemCount() {
        return this.videoModelArrayList != null ? this.videoModelArrayList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {

        Object recyclerViewItem = videoModelArrayList.get(position);
        if (recyclerViewItem instanceof UnifiedNativeAd || recyclerViewItem == null) {
            //Este tipo de objeto corresponde a los anuncios
            return Global.UNIFIED_NATIVE_AD_VIEW_TYPE;
        }else if(recyclerViewItem instanceof  Item) {
            //Este tipo de item es cuando  se realiza una busqueda
            return Global.ITEM_VIDEO_SEARCH_TYPE;
        }else if (recyclerViewItem instanceof  PlayList){
            //Son las playlist creadas por el usuario
            return  Global.ITEM_PLAYLIST_TYPE;
        }else if(recyclerViewItem instanceof  DetailPlayList){
            //Corresponde al tipo de video que esta guardado en una playlist  (Videos de una playtlist)
            return  Global.ITEM_DETAIL_PLAYLIST_TYPE;
        }else{
            //Typo de video de  Favoritos, Ver mas tarde o Historial
            return  Global.ITEM_PLAYLIST_VIDEO_TYPE;
        }
    }
    public void updateRecycler(List<Object> data){
        this.videoModelArrayList.clear();
        this.videoModelArrayList.addAll(data);
        this.notifyDataSetChanged();

    }
    private class YoutubeViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle, tvDescription;
        private ImageView imgVideo;
        private LinearLayout containerItem;


        private YoutubeViewHolder(View itemView) {
            super(itemView);
            imgVideo = itemView.findViewById(R.id.img_playlist);
            tvDescription = itemView.findViewById(R.id.tv_description);
            tvTitle = itemView.findViewById(R.id.tv_title);
            containerItem = itemView.findViewById(R.id.container_item);

        }
    }
}
