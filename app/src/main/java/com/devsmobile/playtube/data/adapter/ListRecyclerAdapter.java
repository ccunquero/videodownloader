package com.devsmobile.playtube.data.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import com.devsmobile.playtube.data.model.PlayList;
import com.devsmobile.playtube.inyection.callback.ISelectItem;
import com.devsmobile.playtube.R;
import me.omidh.liquidradiobutton.LiquidRadioButton;

public class ListRecyclerAdapter extends RecyclerView.Adapter<ListRecyclerAdapter.ViewHolder> {

    private ISelectItem iSelectItem;
    private List<PlayList> playListList;
    private ViewHolder lastViewHolderSelected;

    public ListRecyclerAdapter(ISelectItem iSelectItemMore, List<PlayList> playLists) {

        this.iSelectItem = iSelectItemMore;
        this.playListList = playLists;


    }

    @NonNull
    @Override
    public ListRecyclerAdapter.ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.item_radiobutton_play_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final PlayList item = this.playListList.get(position);


        holder.radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (lastViewHolderSelected != null) {
                    lastViewHolderSelected.radioButton.setChecked(false);
                }
                lastViewHolderSelected = holder;
                holder.radioButton.setChecked(true);

                iSelectItem.onSelectedItem(item, position);
            }
        });
        holder.radioButton.setText(item.getTitle());


    }

    @Override
    public int getItemCount() {
        return this.playListList != null ? this.playListList.size() : 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        LiquidRadioButton radioButton;

        private ViewHolder(View itemView) {
            super(itemView);
            radioButton = itemView.findViewById(R.id.radio_option);


        }
    }
}