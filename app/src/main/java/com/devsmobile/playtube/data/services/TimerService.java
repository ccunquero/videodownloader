package com.devsmobile.playtube.data.services;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.widget.Toast;

import java.io.Serializable;
import java.util.Timer;
import java.util.TimerTask;

public class TimerService extends Service
{
    private Timer timer = new Timer();
    private Messenger messageHandler;
    private int counter = 0;
    public TimerService(){
        super();
    }


    @Override
    public IBinder onBind(Intent intent)
    {


        return null;
    }

    @Override
    public void onCreate()
    {
        super.onCreate();
        startService();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Bundle extras = intent.getExtras();
        messageHandler = (Messenger) extras.get("MESSENGER");
        counter = 0;
        return super.onStartCommand(intent, flags, startId);
    }

    private void startService()
    {
        timer.scheduleAtFixedRate(new mainTask(), 0, 240000);
    }

    private class mainTask extends TimerTask implements Serializable
    {
        @Override
        public void run()
        {
            Message message = Message.obtain();

            try {
                if(messageHandler != null){
                    messageHandler.send(message);

                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }

        }


    }

    public void onDestroy()
    {
        super.onDestroy();
   //     Toast.makeText(this, "Service Stopped ...", Toast.LENGTH_SHORT).show();
        messageHandler = null;
        counter = 0;
    }

}