package com.devsmobile.playtube.data.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.ads.formats.UnifiedNativeAd;
import com.google.gson.internal.LinkedTreeMap;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.List;

import com.devsmobile.playtube.data.model.Item;
import com.devsmobile.playtube.inyection.callback.IPlayVideo;
import com.devsmobile.playtube.R;
import com.devsmobile.playtube.ui.main.Common;
import com.devsmobile.playtube.util.Global;
import com.devsmobile.playtube.util.HelperData;
import com.devsmobile.playtube.util.Image;

public class VideoChannelRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder  > {
    private Context context;
    private List<Object> videoModelArrayList;
    private IPlayVideo iPlayVideo;

    public VideoChannelRecyclerAdapter(Context context, List<Object> videoModelArrayList, IPlayVideo iPlayVideo) {
        this.context = context;
        this.videoModelArrayList = videoModelArrayList;
        this.iPlayVideo = iPlayVideo;

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == Global.UNIFIED_NATIVE_AD_VIEW_TYPE) {
            LayoutInflater layoutInflaterUnified = LayoutInflater.from(parent.getContext());
            View viewUnified = layoutInflaterUnified.inflate(R.layout.ad_unifed_big, parent, false);
            return new UnifiedNativeAdViewHolder(viewUnified);
        }else{

            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.item_video_channel_list, parent, false);
            return new YoutubeViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder recyclerHolder, final int position) {
        int viewType = getItemViewType(position);

        if (viewType == Global.UNIFIED_NATIVE_AD_VIEW_TYPE) {
            UnifiedNativeAd nativeAd = (UnifiedNativeAd) videoModelArrayList.get(position);
            new Common(context).populateNativeAdViewBig(nativeAd, ((UnifiedNativeAdViewHolder) recyclerHolder).getAdView());
        } else {
            YoutubeViewHolder holder  = (YoutubeViewHolder) recyclerHolder;
            final Item itemModel = (Item) videoModelArrayList.get(position);

            if (itemModel.getStatistics() == null) return;

            long viewCont ;
            try{
                 viewCont = Long.parseLong(itemModel.getStatistics().getViewCount());
            }catch (Exception ex){
                viewCont = 0;
            }

            holder.tvTitle.setText(itemModel.getSnippet().getTitle());
            try {
                holder.tvTime.setText(new HelperData().durationFormatter(itemModel.getContentDetails().getDuration()));
            } catch (Exception ex) {
                System.out.println(ex.toString());
                holder.tvTime.setText(R.string.time_zero);
            }
            holder.tvViews.setText(new HelperData().getNumberFormatter(viewCont) + " "+ context.getString(R.string.view));

            Image.loadImage(holder.imgVideo, itemModel.getSnippet().getThumbnails().getMedium().getUrl());

            holder.imgVideo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    LinkedTreeMap<Object, Object> id = (LinkedTreeMap) itemModel.getId();

                    if (id.get("videoId") != null) {
                        Bundle bundle = new Bundle();
                        bundle.putString("VIDEO_ID", id.get("videoId").toString());
                        bundle.putString("VIDEO_TITLE", itemModel.getSnippet().getTitle());
                        bundle.putLong("VIDEO_LIKE", itemModel.getStatistics().getLikeCount() != null ? Long.parseLong(itemModel.getStatistics().getLikeCount()) : 0);
                        bundle.putLong("VIDEO_DISLIKE", itemModel.getStatistics().getDislikeCount() != null ? Long.parseLong(itemModel.getStatistics().getDislikeCount()) : 0);
                        bundle.putLong("VIDEO_VIEW", itemModel.getStatistics().getViewCount() != null ? Long.parseLong(itemModel.getStatistics().getViewCount()) : 0);
                        bundle.putString("VIDEO_DESCRIPTION", itemModel.getSnippet().getDescription());
                        bundle.putString("CHANNEL_ID", itemModel.getSnippet().getChannelId());
                        bundle.putString("VIDEO_URL_THUMBNAIL", itemModel.getSnippet().getThumbnails().getMedium().getUrl());
                        iPlayVideo.onPlayVideo(bundle);
                    }
                }
            });
            holder.btnMore.setOnClickListener(new View.OnClickListener() {
                @SuppressLint("RestrictedApi")
                @Override
                public void onClick(View v) {

                }
            });

        }


    }
    @Override
    public int getItemViewType(int position) {

        Object recyclerViewItem = videoModelArrayList.get(position);
        if (recyclerViewItem instanceof UnifiedNativeAd) {
            return Global.UNIFIED_NATIVE_AD_VIEW_TYPE;
        }else{
            return Global.ITEM_VIEW_TYPE;
        }
    }

    public void updateRecycler(List<Object> data){
        this.videoModelArrayList.clear();
        this.videoModelArrayList.addAll(data);
        this.notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return videoModelArrayList != null ? videoModelArrayList.size() : 0;
    }

    private class YoutubeViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle, tvViews, tvTime;
        private ImageButton btnMore;
        private RoundedImageView imgVideo;


        private YoutubeViewHolder(View itemView) {
            super(itemView);
            imgVideo = itemView.findViewById(R.id.img_video);
            tvTitle = itemView.findViewById(R.id.tv_title);
            tvViews = itemView.findViewById(R.id.tv_views);
            tvTime = itemView.findViewById(R.id.tv_time);
            btnMore = itemView.findViewById(R.id.btn_more);

        }
    }
}
