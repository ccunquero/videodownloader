package com.devsmobile.playtube.data.model;

public class Video {
    private int idVideo;
    private String idVideoYouTube;
    private String titleVideo;
    private String channelName;
    private String urlThumbnail;
    private String idChannel;
    private String type;


    public String getIdVideoYouTube() {
        return idVideoYouTube;
    }

    public void setIdVideoYouTube(String idVideoYouTube) {
        this.idVideoYouTube = idVideoYouTube;
    }

    public String getTitleVideo() {
        return titleVideo;
    }

    public void setTitleVideo(String titleVideo) {
        this.titleVideo = titleVideo;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getUrlThumbnail() {
        return urlThumbnail;
    }

    public void setUrlThumbnail(String urlThumbnail) {
        this.urlThumbnail = urlThumbnail;
    }

    public String getIdChannel() {
        return idChannel;
    }

    public void setIdChannel(String idChannel) {
        this.idChannel = idChannel;
    }

    public int getIdVideo() {
        return idVideo;
    }

    public void setIdVideo(int idVideo) {
        this.idVideo = idVideo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
