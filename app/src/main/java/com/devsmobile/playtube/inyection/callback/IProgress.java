package com.devsmobile.playtube.inyection.callback;

public interface IProgress {
    void onInit();

    void onProgress(float progress, long eatSeconds);

    void onFinish();

    void onFailure();
}
