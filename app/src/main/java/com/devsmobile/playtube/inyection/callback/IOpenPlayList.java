package com.devsmobile.playtube.inyection.callback;

import android.os.Bundle;

public interface IOpenPlayList {
    void onShowPlayList(Bundle bundle);
}

