package com.devsmobile.playtube.inyection.callback;

public interface ILoading {
    void onInitLoading();

    void onFinishLoading();
}
