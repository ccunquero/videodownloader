package com.devsmobile.playtube.inyection.callback;

import android.graphics.Bitmap;

public interface IGetImage {

    void onGetImage(Bitmap bitmap);

    void onErrorImage(Throwable throwable);
}
