package com.devsmobile.playtube.inyection.callback;

public interface IClick {
    void onClickListener(Object object);
}
