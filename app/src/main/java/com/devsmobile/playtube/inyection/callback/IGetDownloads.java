package com.devsmobile.playtube.inyection.callback;

public interface IGetDownloads {
    void getDownloads();
}
