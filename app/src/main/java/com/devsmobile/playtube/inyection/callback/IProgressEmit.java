package com.devsmobile.playtube.inyection.callback;

public interface IProgressEmit {
    void onInit();

    void onProgress(float progress, long eatSeconds);

    void onFinish();

    void onFailure();
}
