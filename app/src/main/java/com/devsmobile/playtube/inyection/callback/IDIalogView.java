package com.devsmobile.playtube.inyection.callback;

import android.view.View;

public interface IDIalogView {
    void newViewDialog(View view);
}
