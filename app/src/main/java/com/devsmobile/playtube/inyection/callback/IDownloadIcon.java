package com.devsmobile.playtube.inyection.callback;


import com.devsmobile.playtube.data.model.Item;

public interface IDownloadIcon {
    void onItemDownloadClick(Item item);
}
