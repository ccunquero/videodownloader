package com.devsmobile.playtube.inyection.callback;

import com.devsmobile.playtube.data.adapter.VideoDownloadsRecyclerAdapter;

public interface IViewHolderItemDownload {
    void onCreateView(String urlVideo, VideoDownloadsRecyclerAdapter.YoutubeViewHolder view);
}
