package com.devsmobile.playtube.inyection.callback;

import java.util.List;

import com.devsmobile.playtube.data.model.Item;

public interface IDataVideoChannel {
    void onGetDataVideoChannelSuccess(List<Item> itemList);

    void onGetDataVideoChannelFailure(Throwable t);
}

