package com.devsmobile.playtube.inyection.api;


import com.devsmobile.playtube.data.model.Main;
import retrofit2.Call;
import retrofit2.http.GET;


public interface IVideo {
    @GET
    Call<Main> getDataVideo(@retrofit2.http.Url String url);

}
