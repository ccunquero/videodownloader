package com.devsmobile.playtube.inyection.callback;

import android.os.Bundle;

public interface IPlayVideo {
    void onPlayVideo(Bundle bundle);
}
