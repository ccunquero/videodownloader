package com.devsmobile.playtube.inyection.callback;

public interface ISelectItem {
    void onSelectedItem(Object item, int index);
}
