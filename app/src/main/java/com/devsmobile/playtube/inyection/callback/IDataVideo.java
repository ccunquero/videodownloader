package com.devsmobile.playtube.inyection.callback;

import com.devsmobile.playtube.data.model.Main;

public interface IDataVideo {
    void onGetDataVideoSuccess(Main main);

    void onGetDataVideoFailure(Throwable t);
}

