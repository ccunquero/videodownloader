package com.devsmobile.playtube.inyection.callback;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;

public interface IYouTubePlayer {
    void onReady(YouTubePlayer youTubePlayer);

    void onError(YouTubePlayer youTubePlayer, PlayerConstants.PlayerError playerError);
}
