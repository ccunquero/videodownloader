package com.devsmobile.playtube.inyection.callback;

import android.os.Bundle;

public interface IOpenDialogChannel {
    void onOpenDialogChannel(Bundle bundle);
}
