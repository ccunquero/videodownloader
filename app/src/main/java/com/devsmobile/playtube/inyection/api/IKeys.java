package com.devsmobile.playtube.inyection.api;


import com.devsmobile.playtube.data.model.Keys;
import com.devsmobile.playtube.data.model.Main;

import retrofit2.Call;
import retrofit2.http.GET;


public interface IKeys {
    @GET
    Call<Keys> getKeys(@retrofit2.http.Url String url);

}
