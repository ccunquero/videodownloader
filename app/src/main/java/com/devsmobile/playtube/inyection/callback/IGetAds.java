package com.devsmobile.playtube.inyection.callback;

import com.google.android.gms.ads.formats.UnifiedNativeAd;

import java.util.List;

public interface IGetAds {
    void onGetSuccessAds(List<UnifiedNativeAd> nativeAdList);
    void onError();
}
