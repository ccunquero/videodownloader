package com.devsmobile.playtube.inyection.callback;

public interface IDownloadVideo {
    void onPreDownload();

    void onProgress(float progress, long eatSeconds);

    void onPostDownload();

    void onFailureDownload();

}
