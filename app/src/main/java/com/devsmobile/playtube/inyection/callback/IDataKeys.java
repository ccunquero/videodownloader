package com.devsmobile.playtube.inyection.callback;

import com.devsmobile.playtube.data.model.Keys;
import com.devsmobile.playtube.data.model.Main;

public interface IDataKeys {
    void onGetDataKeysSuccess(Keys main);
    void onGetDataKeysFailure(Throwable t);
}

