package com.devsmobile.playtube.util;


public class Url {

    //Example :  https://www.googleapis.com/youtube/v3/videos?id=VhqJT1HcWt0&part=statistics&key=AIzaSyAjGIuysJFYLQjw3HXpWSeSqukNDtumxRA
    public String getUrlSingleVideo(String apiKey, String videoID, String part) {
        return Global.URL_BASE + "videos?id=" + videoID
                + "&key=" + apiKey + "&part=" + part;
    }

    //Example : https://www.googleapis.com/youtube/v3/videos?chart=mostPopular&key=AIzaSyDV12DQl2HdgA38A0gn8TNbksqJOmdzXwc&part=id,snippet,contentDetails,statistics&maxResults=2&regionCode=gt&pageToken=CAIQAA
    public String getUrlChartVideo(String chartVideo, String apiKey, String part, int maxResults, String regionCode, String idCategory, String pageToken) {

       String category =  idCategory.length() > 0 ? idCategory : "0";
       String nextPageToken = pageToken != null ? "&pageToken=" + pageToken : "";
        return Global.URL_BASE + "videos?chart=" + chartVideo +
                "&key=" + apiKey + "&part=" + part + "&maxResults=" + maxResults + "&regionCode=" + regionCode + "&videoCategoryId=" + category + nextPageToken;
    }

    //Example : https://www.googleapis.com/youtube/v3/search?&part=id,snippet&maxResults=10&key=AIzaSyDV12DQl2HdgA38A0gn8TNbksqJOmdzXwc&q=hola
    public String getUrlQuery(String query, String apiKey, String part, String type, int maxResults, String idVideoRelated, String pageToken) {

        String related = idVideoRelated.length() > 0 ? "&relatedToVideoId=" + idVideoRelated : "";
        String nextPageToken = pageToken != null ? "&pageToken=" + pageToken : "";

        return Global.URL_BASE + "search?part=" + part + "&q=" + query + "&key=" + apiKey + "&maxResults=" + maxResults + "&type=" + type + related + nextPageToken;
    }


    public String getUrlPlayListDetails(String part, String apiKey, String id) {
        return Global.URL_BASE + "/playlists?part=" + part + "&id=" + id + "&key=" + apiKey;

    }

    //Example : https://www.googleapis.com/youtube/v3/channels?part=brandingSettings&key=AIzaSyDV12DQl2HdgA38A0gn8TNbksqJOmdzXwc&id=UCWOA1ZGywLbqmigxE4Qlvuw%22&fields=items(id%2CbrandingSettings)
    public String getUrlsChannelsBanner(String apiKey, String part, String id) {
        return Global.URL_BASE + "channels?part=" + part + "&key=" + apiKey + "&id=" + id + "&fields=items(id%2CbrandingSettings)";
    }

    //Example : https://www.googleapis.com/youtube/v3/channels?part=id,snippet,statistics&key=AIzaSyDV12DQl2HdgA38A0gn8TNbksqJOmdzXwc&id=UCWOA1ZGywLbqmigxE4Qlvuw
    public String getUrlChannelData(String apiKey, String part, String id) {
        return Global.URL_BASE + "channels?part=" + part + "&key=" + apiKey + "&id=" + id;
    }

    //Example : https://www.googleapis.com/youtube/v3/channels?part=id,snippet,brandingSettings,statistics&key=AIzaSyDV12DQl2HdgA38A0gn8TNbksqJOmdzXwc&id=UCmr0m5RQoEDXQY3hFBIEK3w&fields=items(id%2Csnippet%2Fthumbnails%2CbrandingSettings%2Cstatistics)
    public String getUrlChannelsAvatarBanner(String apiKey, String part, String id) {
        return Global.URL_BASE + "channels?part=" + part + "&key=" + apiKey + "&id=" + id + "&fields=items(id%2Csnippet%2Fthumbnails%2CbrandingSettings%2Cstatistics)";
    }

    //Example : https://www.googleapis.com/youtube/v3/search?order=date&part=id&channelId=UCpd3rXPsHKu1nRnUyxEm1xQ&maxResults=2&key=AIzaSyDV12DQl2HdgA38A0gn8TNbksqJOmdzXwc
    public String getUrlIdVideoChannel(String order, String part, String key, String channelID, int maxResult, String pageToken) {

        String nextPageToken = pageToken != null ? "&pageToken=" + pageToken : "";
        return Global.URL_BASE + "search?order=" + order + "&part=" + part + "&key=" + key + "&channelId=" + channelID + "&maxResults=" + maxResult + "&type=video" + nextPageToken;
    }

    //Example : https://www.googleapis.com/youtube/v3/videos?part=statistics,contentDetails&id=FKXSh14svlQ,_zbcn3NChs8&key=AIzaSyDV12DQl2HdgA38A0gn8TNbksqJOmdzXwc
    public String getUrlDataVideoChannel(String part, String key, String id) {
        return Global.URL_BASE + "videos?part=" + part + "&key=" + key + "&id=" + id;
    }

    //Example : https://www.googleapis.com/youtube/v3/playlistItems?part=id,%20snippet,status&playlistId=PLguR-nVYiO9zl3EUcrtA24shL50kfzep6&key=AIzaSyDV12DQl2HdgA38A0gn8TNbksqJOmdzXwc&maxResults=10

    public String getUrlPlayListItems(String part, String key, String id, int maxResults) {
        return Global.URL_BASE + "playlistItems?part=" + part + "&playlistId=" + id + "&key=" + key + "&maxResults=" + maxResults;
    }

    //Example : https://www.googleapis.com/youtube/v3/videoCategories?key=AIzaSyDV12DQl2HdgA38A0gn8TNbksqJOmdzXwc&part=id,snippet&regionCode=gt&hl=es
    public String getUrlCategories(String part, String regionCode, String language, String key) {
        return Global.URL_BASE + "videoCategories?part=" + part + "&regionCode=" + regionCode + "&hl=" + language + "&key=" + key;
    }
}

