package com.devsmobile.playtube.util;

import com.google.gson.internal.LinkedTreeMap;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.devsmobile.playtube.data.model.ContentDetails;
import com.devsmobile.playtube.data.model.Item;
import com.devsmobile.playtube.data.model.Main;
import com.devsmobile.playtube.data.model.Statistics;

public class HelperData {

    public List<Item> getDataChannelURL(List<Item> itemList, Main response) {
        List<Item> listResponse = response.getItems();

        if(listResponse != null){
            for (int i = 0; i < response.getItems().size(); i++) {
                for (int j = 0; j < listResponse.size(); j++) {

                    if (itemList.get(i).getSnippet().getChannelId().equals(listResponse.get(j).getId())) {
                        Statistics statistics = itemList.get(i).getStatistics();
                        if (statistics == null) {
                            statistics = new Statistics();
                        }
                        statistics.setViewCountChannel(listResponse.get(j).getStatistics().getViewCount());
                        statistics.setSubscriberCount(listResponse.get(j).getStatistics().getSubscriberCount());
                        statistics.setVideoCount(listResponse.get(j).getStatistics().getVideoCount());

                        itemList.get(i).getSnippet().setUrlAvatar(listResponse.get(j).getSnippet().getThumbnails().getDefault().getUrl());

                        int finalI = i;
                        int finalJ = j;
                        Thread thread = new Thread(new Runnable() {

                            @Override
                            public void run() {
                                try  {
                                  String urlBanner =   new Image().getYoutubeChannelHeaderImage("https://www.youtube.com/c/" + listResponse.get(finalJ).getBrandingSettings().getChannel().getTitle());
                                    itemList.get(finalI).getSnippet().setUrlBanner(urlBanner);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });

                        thread.start();
                        itemList.get(i).getSnippet().setChannelProfileColor(listResponse.get(j).getBrandingSettings().getChannel().getProfileColor());
                        itemList.get(i).setStatistics(statistics);
                    }
                }
            }
        }
        return itemList;
    }

    public List<Item> getDataWithInfoVideo(List<Item> itemList, List<Item> responseItems) {
        for (int i = 0; i < itemList.size(); i++) {
            for (int j = 0; j < responseItems.size(); j++) {
                String id = null;
                if (itemList.get(i).getId() instanceof String) { // Cuando es playList
                    id = itemList.get(i).getSnippet().getResourceId().getVideoId();
                } else {
                    LinkedTreeMap<Object, Object> idVideo = (LinkedTreeMap) itemList.get(i).getId();
                    if (idVideo.get("videoId") != null) {
                        id = idVideo.get("videoId").toString();
                    }
                }

                String id1 = (String) responseItems.get(j).getId();
                if (id != null) {
                    if (id.equals(id1)) {
                        ContentDetails currentContentDetails = responseItems.get(j).getContentDetails();
                        itemList.get(i).setStatistics(responseItems.get(j).getStatistics());
                        itemList.get(i).setContentDetails(currentContentDetails);
                    }
                }

            }
        }
        return itemList;
    }

    public StringBuilder getID(List<Item> itemList, Boolean isObjectId) {
        StringBuilder id = new StringBuilder();
        for (int i = 0; i < itemList.size(); i++) {
            Item currentItem = itemList.get(i);
            if (!isObjectId)
                id.append(currentItem.getSnippet().getChannelId());
            else {
                if (currentItem.getId() instanceof String) { //Cuando es play list
                    id.append(currentItem.getSnippet().getResourceId().getVideoId());
                } else {
                    LinkedTreeMap<Object, Object> object = (LinkedTreeMap) currentItem.getId();
                    if (object.get("videoId") != null)
                        id.append(object.get("videoId").toString());
                }


            }
            if (i < itemList.size() - 1) {
                id.append(",");
            }
        }
        return id;
    }

    public String getNumberFormatter(long number) {
        long numberDivider = 0;
        String sufix = "";
        if (number < 1000) {
            sufix = "";
            numberDivider = 1;
        } else if (number >= 1000 && number < 1000000) {
            sufix = " K";
            numberDivider = 1000;
        } else if (number >= 1000000) {
            sufix = " M";
            numberDivider = 1000000;
        }

        BigDecimal bd = new BigDecimal((double) number / (double) numberDivider).setScale(1, RoundingMode.HALF_UP);
        double newInput = bd.doubleValue();
        String auxNumber = String.valueOf(newInput);


        int p = auxNumber.indexOf('.');
        if (p >= 0) {
            String left = auxNumber.substring(0, p);
            String right = auxNumber.substring(p + 1);

            if (right.charAt(0) == '0') {
                auxNumber = left;
            }

        }
        return auxNumber + sufix;

    }

    private long getDuration(String strDuration) {
        String time = strDuration.substring(2);
        long duration = 0L;
        Object[][] indexs = new Object[][]{{"H", 3600}, {"M", 60}, {"S", 1}};
        for (int i = 0; i < indexs.length; i++) {
            int index = time.indexOf((String) indexs[i][0]);
            if (index != -1) {
                String value = time.substring(0, index);
                duration += Integer.parseInt(value) * (int) indexs[i][1] * 1000;
                time = time.substring(value.length() + 1);
            }
        }
        return duration;
    }

    public String durationFormatter(String strDuration) {

        long millis = getDuration(strDuration);
        String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(millis),
                TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
        //00:00:00
        if (hms.substring(3, 5).equals("00")) {
            hms = hms.substring(6) + " seg.";
        } else if (hms.substring(0, 2).equals("00")) {
            hms = hms.substring(3) + " min.";
        } else {
            hms = hms + " hrs";
        }
        return hms;
    }
    public static String getApiKey(){
        Random rand = new Random();

        int index = rand.nextInt(Global.API_KEYS.length -1);
        return Global.API_KEYS[index];
    }

}
