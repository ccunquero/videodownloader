package com.devsmobile.playtube.util;

import android.os.Environment;

import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;

import java.io.File;
import java.util.List;

import com.devsmobile.playtube.data.model.Item;

public class Global {
    //Key para acceder realizar peticiones a la API de Youtube
    //public static final String API_KEY = "AIzaSyDV12DQl2HdgA38A0gn8TNbksqJOmdzXwc";
    //  public static final String API_KEY = "AIzaSyBkNrlvpicjwvjxrwxu75bx-Vte392Y5OE";
   // public static final String API_KEY = "AIzaSyDSqkDkW4WI0Cf3Q0TJfRniz77Yx_fOfJc";
    //Url inicial que se utiliza en todas las peticiones
    public static final String URL_BASE = "https://www.googleapis.com/youtube/v3/";
    public static final String URL_API = "https://play-tube-gx.herokuapp.com/";
    //parametros en la url que se puede enviar para obtener la data
    public static final String ALL_PART = "id,snippet,contentDetails,statistics";
    public static final String ID_PART = "id";
    public static final String SNIPPET_PART = "snippet";
    public static final String CONTENT_DETAILS_PART = "contentDetails";
    public static final String STATISTICS_PART = "statistics";
    public static final String STATUS_PART = "status";
    public static final String CHART = "mostPopular";
    public static final String ALL_TYPES = "video,channel,playlist";
    public static final String TYPE_VIDEO = "video";
    public static final String KIND_PLAYLIST = "youtube#playlist";
    public static final String KIND_VIDEO = "youtube#video";
    public static final String KIND_CHANNEL = "youtube#channel";
    public static final String CHANNEL_PART = "id,snippet,brandingSettings,statistics";
    public static final String DATE_PART = "date";
    //Estas constantes eran utilizadas en las descargas (Ahora ya no se utilizan)
    public static final String FILE_NAME_STATUS_DOWNLOAD = "FileDownloadStatus.txt";
    public static final String FOLDER_TEMP = "temp";
    public static final String URL_BASE_YOUTUBE = "https://www.youtube.com/watch?v=";
    public static final String DATABASE_NAME = "DbDownload";
    public static final String DOWNLOAD_IN_HOLD = "IN_HOLD";
    public static final String DOWNLOAD_IN_PROGRESS = "IN_PROGRESS";
    public static final String DOWNLOAD_FINISHED = "FINISHED";
    public static final String DOWNLOAD_FAILURE = "FAILURE";
    public static final String SHARED_PREFERENCES_NAME = "DownloadPreferences";
    //Version de la base datos SQLite
    public static final int DATABASE_VERSION = 11;
    //Nombre de  tablas de la db
    public static final String TABLE_DOWNLOAD = "tblDownloads";
    public static final String TABLE_PLAY_LIST = "tblPlayList";
    public static final String TABLE_DETAIL_PLAY_LIST = "tblDetailPlayList";
    public static final String TABLE_VIDEO = "tblVideo";
    //Tipos de videos que se guardan en la tabla  : tblVideo
    public static final String TYPE_VIDEO_FAVORITE = "FAVORITE";
    public static final String TYPE_VIDEO_HISTORY = "HISTORY";
    public static final String TYPE_VIDEO_SEE_LATER = "SEE LATER";
    //Tipo de playlist, (Es utlizada en la clase PlayListRecyclerAdapter)
    public static final String PLAY_LIST_IN_SEARCH = "PLAY_LIST_IN_SEARCH";
    public static final String PLAY_LIST_USER_CREATED = "PLAY_LIST_USER_CREATED";
    public static final String PLAY_LIST_DETAIL = "PLAY_LIST_DETAIL";
    public static final String PLAY_LIST_VIDEO = "PLAY_LIST_VIDEO";
    public static final String PLAYING = "PLAYING";
    public static final String PAUSED = "PAUSED";
    public static final int NOTIFICATION_ID = 1;
    public static final String CHANNEL_NOTIFICATION_ID = "CHANNEL_1";
    public static final String CHANNEL_NAME = "CHANNEL_PLAY_VIDEO";
    public static final String CHANNEL_DESCRIPTION = "PLAY VIDEO";
    public static int MAX_RESULTS = 4;
    public static int MAX_RESULTS_PLAY_LIST = 20;
    public static String PATH_DOWNLOAD = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + File.separator + "DownloadsYoutube";
    public static boolean IS_DONLOADING = false;
    //Son utlizadas en la reproduccion del video
    public static YouTubePlayer YOUTUBE_PLAYER;
    public static String STATE_VIDEO;
    public static boolean PLAY_VIDEO_ACTIVITY_VISIBLE = false;

    public static final String  BROADCAST_CONTENT_LIVE = "live";
    public static int CURRENT_TAB_INDEX_SELECTED = 0;
    public static List<Item> CATEGORIES_VIDEO_DATA ;

    public static final String EMPTY_STRING= "";
    public static final int ZERO_VALUE = 0;

    //tipos de Objeto para los recyclerView
    public static final int ITEM_VIEW_TYPE = 0;
    public static final int UNIFIED_NATIVE_AD_VIEW_TYPE = 1;
    public static final int ITEM_PLAYLIST_TYPE = 2;
    public static final int ITEM_PLAYLIST_VIDEO_TYPE = 3;
    public static final int ITEM_DETAIL_PLAYLIST_TYPE = 4;
    public static final int ITEM_VIDEO_SEARCH_TYPE = 5;

    public static  int SIZE_ADS_NORMAL = 1;
    public static int MAX_RESULT_SEARCH = 8;

    public static boolean PUBLISH_TO_PLAY_STORE = true;
    public static  boolean START_SERVICE_ADS = true;
    public static String ID_ADSSPACE = "130940475";
    //public static String ID_ADSSPACE = "130626426"; // Test

    public static String PUBLISHER_ID = "1100046240";

    public static String ZONE_ID_COLONY = "vz4909ebd3a6c947ac9c";
    public static String APP_ID_MOPUB = "b195f8dd8ded45fe847ad89ed1d016da";

    public static  String[] API_KEYS = {

    };




}