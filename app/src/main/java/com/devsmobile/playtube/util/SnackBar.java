package com.devsmobile.playtube.util;

import android.content.Context;

import com.chootdev.csnackbar.Align;
import com.chootdev.csnackbar.Duration;
import com.chootdev.csnackbar.Snackbar;
import com.chootdev.csnackbar.Type;
import com.devsmobile.playtube.R;


public class SnackBar {

    public static void success(Context context, String message) {
       /* ChocoBar.builder().setActivity(activity)
                .setText(message)
                .setDuration(ChocoBar.LENGTH_SHORT)
                .green()
                .show();

        */
       try{
           Snackbar.with(context, null)
                   .type(Type.SUCCESS)
                   .message(message)
                   .duration(Duration.SHORT)
                   .fillParent(true)
                   .textAlign(Align.LEFT)
                   .show();

       }catch (Exception ex ){
           System.out.println("ERROR NOTIFICATION  SUCCES ");
       }



    }

    public static void info(Context context, String message) {
      /*  ChocoBar.builder().setActivity(activity)
                .setText(message)
                .setDuration(ChocoBar.LENGTH_SHORT)
                .cyan()
                .show();*/

      try{
          Snackbar.with(context, null)
                  .type(Type.UPDATE)
                  .message(message)
                  .duration(Duration.SHORT)
                  .fillParent(true)
                  .textAlign(Align.LEFT)
                  .show();
      }catch (Exception ex ){
          System.out.println();

      }



    }

    public static void error(Context context, String message) {
      /*  ChocoBar.builder().setActivity(activity)
                .setText(message)
                .setDuration(ChocoBar.LENGTH_INDEFINITE)
                .red()
                .show();*/

        try{
            Snackbar.with(context, null)
                    .type(Type.ERROR)
                    .message(message)
                    .duration(Duration.SHORT)
                    .fillParent(true)
                    .textAlign(Align.LEFT)
                    .show();


        }catch (Exception ex){

        }

    }

    public static void errorData(Context context) {
      /*  ChocoBar.builder().setActivity(activity)
                .setText("Se ha producido un error")
                .setDuration(ChocoBar.LENGTH_SHORT)
                .red()
                .show();*/

        try{

            Snackbar.with(context, null)
                    .type(Type.ERROR)
                    .message(context.getString(R.string.error_general))
                    .duration(Duration.SHORT)
                    .fillParent(true)
                    .textAlign(Align.LEFT)
                    .show();


        }catch (Exception ex){

        }

    }

    public static void errorConnection(Context context) {
        Snackbar.with(context, null)
                .type(Type.ERROR)
                .message(context.getString(R.string.error_network))
                .duration(Duration.SHORT)
                .fillParent(true)
                .textAlign(Align.LEFT)
                .show();
    }

}
