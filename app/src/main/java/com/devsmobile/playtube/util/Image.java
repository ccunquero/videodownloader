package com.devsmobile.playtube.util;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import com.devsmobile.playtube.R;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Image {
    public static void loadImage(ImageView image, String url) {
        url = url == null || url.length() == 0 ? "null" : url;
        Picasso.get()
                .load(url)
                .placeholder(R.drawable.placeholder_img)
                .error(R.drawable.placeholder_img)
                .into(image);
    }

    public final static String YOUTUBE_HEADER_IMAGE_START_URL = "yt3.ggpht.com/";
    public final static String YOUTUBE_HEADER_IMAGE_END_URL = "-no-nd-rj";

    public String getYoutubeChannelHeaderImage(String channelUrl) throws Exception
    {

        Document document = Jsoup.connect(channelUrl).userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1").get();

        String html = document.toString();
        Pattern pattern = Pattern.compile(YOUTUBE_HEADER_IMAGE_START_URL + "(.*?)" + YOUTUBE_HEADER_IMAGE_END_URL, Pattern.DOTALL);
        Matcher matcher = pattern.matcher(html);
        while (matcher.find()) {
            String imgUrl = matcher.group(1);
            if (imgUrl.length()<500)
                return "https://" + YOUTUBE_HEADER_IMAGE_START_URL+imgUrl+YOUTUBE_HEADER_IMAGE_END_URL ;
        }

        return  "";
    }
}
